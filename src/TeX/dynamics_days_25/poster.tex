% Derived from learning10poster.tex, ds13.tex and scipy2016.tex.
% Documetnation for poster mode at
% http://www.tex.ac.uk/ctan/macros/latex/contrib/textpos/textpos.pdf

\newcommand{\field}[1]{\mathbb{#1}}
\newcommand\REAL{\field{R}}
\newcommand\INTEGER{\field{Z}}
\newcommand{\EV}[2]{\field{E}_{#1}\left[#2\right]}
\newcommand{\argmax}{\operatorname*{argmax}}
\newcommand{\plotsize}{\tiny} % Size of font in labels
\newcommand\given{\mid}

%\documentclass[a0,showboxes]{a0poster}
\documentclass[a0]{a0poster}
\usepackage{amsmath}
\usepackage{amsfonts}
%\usepackage{graphix}
\pagestyle{empty}
\setcounter{secnumdepth}{0}

% The textpos package is necessary to position textblocks at arbitary 
% places on the page.
\usepackage[absolute]{textpos}

% Graphics to include graphics. Times is nice on posters, but you
% might want to switch it off and go for CMR fonts.
%\usepackage{graphics,wrapfig,times}
\usepackage[pdftex]{rotating} %AMF

% These colours are tried and tested for titles and headers. Don't
% over use color!
\usepackage{color}
%\definecolor{DarkBlue}{rgb}{0.1,0.1,0.5}
\definecolor{DarkBlue}{rgb}{0.1,0.1,0.8}
\definecolor{Red}{rgb}{0.9,0.0,0.1}

% see documentation for a0poster class for the size options here
\let\Textsize\normalsize
\def\Head#1{\noindent\hbox to \hsize{\hfil{\LARGE\color{DarkBlue} #1}}\bigskip}
\def\LHead#1{\noindent{\LARGE\color{DarkBlue} #1}\bigskip}
\def\Subhead#1{\noindent{\large\color{DarkBlue} #1}\bigskip}
\def\Title#1{\noindent{\VeryHuge\color{Red} #1}}

\TPGrid[50mm,50mm]{23}{16}      % 4 cols of width 5, plus 3 gaps

% Well I need more space so the gaps are only .5
\newcommand{\ColumnOne}{0}
\newcommand{\ColumnTwo}{5.5}
\newcommand{\ColumnThree}{11.5}
\newcommand{\ColumnFour}{17.5}
\parindent=0pt
\parskip=0.5\baselineskip

\usepackage{multicol}
\begin{document}

% Understanding textblocks is the key to being able to do a poster in
% LaTeX. In
%
%    \begin{textblock}{wid}(x,y)
%    ...
%    \end{textblock}
%
% the first argument gives the block width in units of the grid
% cells specified above in \TPGrid; the second gives the (x,y)
% position on the grid, with the y axis pointing down.

\begin{textblock}{13}(\ColumnOne, 0) \Title{Entropy, Cross Entropy and Data Assimilation}
\end{textblock}

\begin{textblock}{4}(15,0.05)
  {\Huge\color{DarkBlue} Andrew M.~Fraser}
  \hspace{3cm}
  {\Large andy@FraserPhysics.com}
\end{textblock}

\begin{textblock}{5.5}(\ColumnOne, 1.3)
  \LHead{ODE $\rightarrow$ Observations}
  \begin{align*}
    \dot x &= F(x) \\
    x[0: 10000] &= \text{integrate}(F, n_t=10000, \Delta_\tau=0.15) \\
    \text{bins} &= [-10,0,10] \\
    y &= G(x) \\
    y[0:10000] &= \text{digitize}(x[0:10000,0], \text{bins}])
  \end{align*}
  \begin{center}
    \resizebox{0.9\textwidth}{!}{\includegraphics{particles_a.pdf}}\\
    \resizebox{0.9\textwidth}{!}{\includegraphics{TSintro.pdf}}
  \end{center}
\end{textblock}

\begin{textblock}{5.5}(\ColumnOne, 9.5)
  \LHead{Data Assimilation:\\Observations $\rightarrow$ States $\in \REAL^3$}\\
  Laser data from Tang and Weiss\\
  Extended Kalman smoothing for state space trajectory estimate
  \begin{equation*}
    \hat x = \argmax_x P(x\given y)
  \end{equation*}\vspace{-2.5cm}
  \begin{center}
    \resizebox{0.75\textwidth}{!}{\includegraphics[width=3cm]{LaserLP5.pdf}}\\
    \resizebox{0.75\textwidth}{!}{\includegraphics{LaserStates.pdf}}
  \end{center}
\end{textblock}

\begin{textblock}{5.5}(\ColumnTwo,1.3) %
  \LHead{HMMs \& Data Assimilation:\medskip\\ %
    Observations $\rightarrow$ States $\in
    \INTEGER$}\\
  Parameters, $\theta$, of Hidden Markov Model (HMM) with states
  $s\in\INTEGER$ and observations $y\in\INTEGER$:
  \begin{description}
  \item[$P_{S \leftarrow S}$:] State to state transition probabilities
  \item[$P_{Y \leftarrow S}$:]  Conditional observation probabilities
  \end{description}
  Estimation algorithms:
  \begin{description}
  \item[Forward Filter:] Conditional probability of states
    $P(x[t]\given y[0:t+1], \theta)$
  \item[MLE Parameters:] (Forward-Backward, also known as Baum-Welch)
    \begin{equation*}
      \hat \theta = \argmax_\theta P(y[0:1000]\given \theta)
    \end{equation*}
  \item[MAP States:] (Viterbi)
    \begin{equation*}
      \hat s[0:1000] = \argmax_{s[0:1000]}P(s[0:1000]\given y[1000], \theta)
    \end{equation*}
  \end{description}
  \begin{center}
    \resizebox{0.6\columnwidth}{!}
  {\input{Markov_dhmm.pdf_t}}
  \resizebox{0.7\textwidth}{!}{\includegraphics{Statesintro.pdf}}
  \end{center}
\end{textblock}

\begin{textblock}{5.5}(\ColumnTwo,12)
  \LHead{Variations on a Theme}\\
  \textbf{Forward data assimilation} alternates between \textbf{update} $\&$
  \textbf{forecast}.\vspace{-1cm}
  \begin{description}
  \item[Update:] \parbox{.8\textwidth}{
    \begin{align*}
      \alpha(x,t) &\equiv P(X[t]=x\given y[0:t+1]) \\
      \alpha(x,t) &\propto a(x,t) P_{Y \leftarrow X}(y[t]\given x)
    \end{align*}
    One must evaluate $\int \alpha(x,t) dx$ to normalize the update.}
  \item[Forecast:] \parbox{.8\textwidth}{
    \begin{align*}
      a(x,t) &\equiv P(X[t]=x\given y[0:t]) \\
      a(x,t) &= \int \alpha(\chi,t-1) P_{X \leftarrow X}(x\given \chi) d \chi
    \end{align*}} \vspace{-1cm}
  \end{description}
  \begin{description}
  \item[Kalman Filter] $P_{X \leftarrow X}$ and $P_{Y \leftarrow X}$
    are linear with Gaussian residuals.
  \item[Extended Kalman Filter] Kalman filter for nonlinear functions
    with local linear approximations.
  \item[Hidden Markov Model] State and observation spaces are finite
    sets.
  \item[Particle Filter] Monte-Carlo for integrals.  Probabilities
    represented by clouds of points.
  \end{description}
\end{textblock}

\begin{textblock}{5.5}(\ColumnThree,1.3)
  \LHead{Entropy and Lyapunov Exponents}\\
  \textbf{Entropy} for true model $P_\mu$
  \begin{equation*}
    h(\mu) \equiv \lim_{n \rightarrow \infty} -\frac{1}{n}
      \EV{\mu}{\log(P_\mu (y[0:n]))}
    \end{equation*}
    For $\forall y[0:n] \in A_\epsilon^{(n)}$, the typical or plausible set
    \begin{align*}
      \frac{-\log(P_\mu (y[0:n]))}{n} &= h \pm \epsilon & 
      \text{definition } h \text{ is the rate that prob} \rightarrow 0. \\
      \text{Pr} \left\{  A_\epsilon^{(n)} \right\} &> 1-\epsilon \\
      \left|  A_\epsilon^{(n)} \right| & \leq e^{n(h+\epsilon)} & 
          h \text{ is the rate that } A_\epsilon^{(n)} \text{grows}.
    \end{align*}
    \textbf{Cross Entropy} of other model $\theta$ wrt true $\mu$ 
    \begin{align*}
    h(\mu||\theta) & \equiv \lim_{n \rightarrow \infty} -\frac{1}{n}
      \EV{\mu}{\log(P_\theta (y[0:n]))} \\
      h(\mu||\theta) - h(\mu) & \geq 0 \quad \text{equality } \rightarrow
                                         \mu = \theta \text{ almost everywhere}
    \end{align*}
    \textbf{Lyapunov exponents}, $\lambda_i$ characterize the
    exponential rates that trajectories converge or diverge.
    Estimate them numerically with Benettin's procedure that requires
    integrating tangent equation.   Work of Ruelle, Pesin,
    Ledrappier, Young says that for the Lorenz system the largest
    exponent is equal to the entropy, ie,
    \begin{equation*}
      h = \lambda_0 \approx 0.906.
    \end{equation*}
    So \textbf{0.906} is a lower \textbf{bound} for the cross entropy
    of a model of time series from the Lorenz system.
    \begin{center}
      \resizebox{1.0\textwidth}{!}{\includegraphics{LikeLor.pdf}}
      Use HMMs with many states to approach the bound.
    \end{center}
\end{textblock}

\begin{textblock}{11}(\ColumnThree,12.5)
  \begin{multicols*}{4}[\LHead{Extended Kalman Filter}\vspace{-1cm}]
    Given a model for states $x$ and observations $y$ in which
    \begin{align*}
      x[t+1] &= f(x[t]) + \eta[t]\\
      y[t] &= g(x[t]) + \epsilon[t]
    \end{align*}
    where $f$ and $g$ are differentiable but perhaps nonlinear and
    $\eta$ and $\epsilon$ are iid Gaussian noise, \emph{extended
      Kalman filtering} (EKF) is the practice of using Gaussians to
    model conditional distributions of states and observations.  One
    propagates the means with the functions $f$ and $g$ and uses the
    derivatives of those functions to calculate covariances.

    For Figures~\ref{fig:ToyStretch}--\ref{fig:ToyH} I added draws
    from Gaussians with scales $\sigma_\eta$ and $\sigma_\epsilon$ to
    the states and observations respectively of Lorenz simulations and
    applied EKFs.
    %\newcommand{\MyFigWidth}{0.2\textwidth}
    \newcommand{\MyFigWidth}{\columnwidth}
    \begin{figure}
      \centering
      \resizebox{\MyFigWidth}{!}{\includegraphics[]{ToyStretch.pdf}}
      \caption{Level sets of conditional Gaussians illustrate forecast
      and update distributions.}
      \label{fig:ToyStretch}
    \end{figure}
    \begin{figure}
      \centering
      \resizebox{\MyFigWidth}{!}{\includegraphics[]{ToyTS1.pdf}}
      \caption{Time series of observations and characterizations of the
        forecast errors.}
      \label{fig:ToyTS1}
    \end{figure}
    \begin{figure}
      \centering
      \resizebox{\MyFigWidth}{!}{\includegraphics[]{ToyH.pdf}}
      \caption{Dependence of cross entropy on state noise,
        $\sigma_\eta$, observation noise, $\sigma_\epsilon$, and the
        time interval between samples, $\tau_s$.  While the slopes on
        the right match the true entropy, the intercepts are negative
        because the models are not optimal.}
      \label{fig:ToyH}
    \end{figure}
  \end{multicols*}
\end{textblock}


\begin{textblock}{5.5}(\ColumnFour,1.3)
  \LHead{Particle Filter}\\
  To find something better than an HMM with zillions of states:
  \begin{itemize}
  \item Cover attractor with boxes, ie, particles
  \item Assign a uniform probability density in each box
  \item Use numerical ODE integration of Lorenz system and its tangent
    to move boxes forward in time
  \item When boxes get too big subdivide them
  \item When boxes overlap and get too numerous, random resample to
    decimate
  \end{itemize}
  \begin{center}
    \resizebox{0.9\textwidth}{!}{\includegraphics{particles_b.pdf}}\\
    \resizebox{0.9\textwidth}{!}{\includegraphics{entropy_particle.pdf}}
  \end{center}
\end{textblock}

\end{document}

%%%---------------
%%% Local Variables:
%%% eval: (LaTeX-mode)
%%% End:
