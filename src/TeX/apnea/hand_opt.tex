
% Notes on modeling heart rate

\documentclass[12pt]{article}
\usepackage{graphicx,color}
\usepackage{amsmath, amsfonts}
\usepackage{placeins}
\usepackage{verbatim}
\usepackage{showlabels}

\newcommand{\field}[1]{\mathbb{#1}}
\newcommand{\INTEGER}{\field{Z}}
\newcommand{\REAL}{\field{R}}
\newcommand{\COMPLEX}{\field{C}}
\newcommand{\id}{\mathbb{I}}
\newcommand{\variance}[1]{\field{V}\left[ #1 \right]}
\newcommand{\normal}[2]{{\cal N}\left(#1,#2 \right)}
\newcommand{\argmax}{\operatorname*{argmax}}
\newcommand{\BestModel}{\emph{two\_ar5\_masked}}

\title{Hand Optimization}
\author{Andrew M.\ Fraser}

\begin{document}
\maketitle

\newcommand{\StudyCaption}[2]{
  \caption{Performance vs #1 on the training records (\emph{a} \emph{b}
    and \emph{c} records excluding defective records).  The models
    have #2.}
}

\newcommand{\StudyFigs}[1]{

\begin{figure}
  \centering
  \resizebox{0.8\textwidth}{!}{\includegraphics{threshold.pdf}}
  \StudyCaption{detection threshold}{#1}
  \label{fig:threshold}
\end{figure}

\begin{figure}
  \centering
    \resizebox{0.8\textwidth}{!}{\includegraphics{errors_vs_ar.pdf}}
    \StudyCaption{autoregressive order}{#1}
  \label{fig:ar_order_study}
\end{figure}

\begin{figure}
  \centering
    \resizebox{0.8\textwidth}{!}{\includegraphics{errors_vs_fs.pdf}}
    \StudyCaption{observations per minute}{#1}
  \label{fig:sample_frequency_study}
\end{figure}

\begin{figure}
  \centering
    \resizebox{0.8\textwidth}{!}{\includegraphics{errors_vs_lpp.pdf}}
    \StudyCaption{cutoff period in seconds of low pass filter for heart rate}{#1}
  \label{fig:lpp_study}
\end{figure}

\begin{figure}
  \centering
    \resizebox{0.8\textwidth}{!}{\includegraphics{errors_vs_rc.pdf}}
    \StudyCaption{center frequency in cpm for extracting respiration signal}{#1}
  \label{fig:rc_study}
\end{figure}

\begin{figure}
  \centering
    \resizebox{0.8\textwidth}{!}{\includegraphics{errors_vs_rw.pdf}}
    \StudyCaption{width in cpm of filter for extracting respiration signal}{#1}
  \label{fig:rw_study}
\end{figure}

\begin{figure}
  \centering
    \resizebox{0.8\textwidth}{!}{\includegraphics{errors_vs_rs.pdf}}
    \StudyCaption{cutoff frequency in cpm for smoothing the
      respiration signal}{#1}
  \label{fig:rs_study}
\end{figure}

\begin{table*}
  \centering
  \input{score.tex}
  \caption[Performance]{Performance of a model that has #1.}
  \label{tab:score}
\end{table*}}

\section{Introduction}
\label{sec:introduction}

Here I document how I fit the model I used for task 2 of the CINC2000
event.

Initially I had hoped to re-win the competition.

For the first edition of the book I developed code for doing Viterbi
decoding on classes.  When I implemented testing, I discovered that
the algorithm failed to calculate the maximum likelihood sequence of
classes, and in fact sometimes failed to run.  I was just lucky that
the code ran and produced a plausible estimate.

McNames won the competition by looking at periodograms of the
data and classifying each minute by hand.  I ended up looking at the
test data and trying to get models to match classification that I did by
hand.  I was not entirely successful.  I ultimately decided that the
goal was pointless and chose to use a single fairly simple model to do
the classification.  The result did not re-win the competition, but it
is simple and I believe satisfactory.

\section{Combining Respiration and Heart Rate}
\label{sec:combination}

I use an HMM with six discrete states and observation models that
combine a vector heart rate and respiration signal.  There are three
states for the apnea class and three states for the normal class.  For
each class one state is reserved for \emph{noise}.  The prior for the
variance for the noise states is $\psi, \nu = 10^6, 10^4$, and the
prior for the non-noise states is
\begin{equation*}
  \psi, \nu =
  \begin{bmatrix}
    4,000 & 0 \\ 0 & 0.6 
  \end{bmatrix}, 10,000. 
\end{equation*}
I optimize the model with 40 Baum Welch iterations.  A summary of the
result appears in Fig.~\ref{fig:viz}.

\begin{figure}
  \centering
  \resizebox{1.0\textwidth}{!}{\includegraphics{viz.pdf}}
  \caption{Topology of the HMM used for task-2 classification.}
  \label{fig:viz}
\end{figure}

The parameters to choose include:
\begin{description}
\item[Parameters:] for the (Vector Autoregressive Gaussian) model
  of the 2-d vectors with components respiration and low-pass filtered
  heart rate.  The parameters include:
  \begin{description}
  \item[AR order] 8 % AR
  \item[sample rate] 4 % FS
  \item[low pass period] 65 seconds % LPP
  \item[respiration pass center] 12 per minute % RC
  \item[respiration pass width] 3.6 per minute % RW
  \item[envelope smooth] .47 per minute % RS
  \item[$\psi$ and $\nu$] Priors for variance in the observation model
    for each state
  \end{description}
\item[Detector threshold] 0.7 % THRESHOLD
\end{description}
I chose the parameter values in foregoing list to get the best
performance\footnote{AR order 10 reduces the error count from 2,270 to
  2,260.  I chose AR order 8 to reduce over fitting.} on the training
data.

Figures \ref{fig:threshold} --- \ref{fig:rs_study}
illustrate the effects of varying several parameters.  I've plotted
the number of classification errors on the training data against the
parameters.  The training records are the \emph{a} records and the
\emph{b} and \emph{c} except \emph{b05, c04,} and \emph{c06} which are
defective\footnote{The ECG for b05 is not usable.  c04 has an
  arrhythmia in which the interval between beats is very short as
  frequently as every third beat.  And c06 is a shifted version of
  c05.}.

\StudyFigs{multiple states and the observation models are
  vector autoregressive models for heart rate and respiration}

\section{Test Results}
\label{sec:results}

\begin{table*}
  \centering
  \input{test_score.tex}
  \caption[Performance]{Performance on the test data.}
  \label{tab:test_score}
\end{table*}

One could explore how performance on the training data and the test
data varies with the number of states in the HMM.

\end{document}

In the book, I got to error rate of .1176 on the training data (1952
errors) and .1305 (2253 errors) on the test data.

Here with 40 training iterations a single 6 state model gets 2270
errors (rate .1449) on training data and 2741 errors (rate .1587) on
the test data.

I tried a single 8 state model and trained it with 100 iterations.  It
got 2207 errors on training data and 2798 errors on the test data.  I
used 100 iterations because the training was still improving after 40.

Trying 100 training iterations on the 6 state model yields 2270 errors
on the training data and 2742 errors on the test data.  I reverted to
40 training iterations.

Here are notes on the apnea data:

x-files with untrimmed beginnings: 01, 04, 06, 18, 22, 24, 26, 28, 29,
35

Guesses about which x-files are ``Normal'' based on looking at results
of explore.py

x04
x06
x11 
x17
x22
x24
x29
x33
x34
x35

x11 has atypical beats, but my QRS code tracks and provides OK heart
rate

x29 has alternans.  I think it's a ``Normal'' record

x26 Has atypical beats and my QRS code occasionally also fails for
normal beats.  I think that the atypical beats queers the training and
leads to the failures on normal beats.

X33 and x34 are shifted measurements of the same event, as are c05 and
c06.

x04 OK after about t=88

x11 QRS-HMM is OK.  Easy to see PVC peaks in ECG.  At coarse time
scale, I can't separate PVC peaks from low frequency excursions.
Solution: Mask positive peaks 3\%, low likelihood 1\%, and long
inter-beat periods (hr=55bpm)

x13 QRS-HMM is OK.  Easy to see PVC valleys in ECG.  Solution: Mask on
negative peaks 5\% and low likelihood 1\%.

x19 QRS-HMM is OK, but Arrhythmia may cause high fit threshold that
throws off classification.  Solution: Mask on low likelihood 10\% and
set threshold to 1.2

x26 QRS-HMM inserts extra beats.  Arrhythmia missing R.  Solution:
Custom alpha-beta variance prior.  Mask on negative peaks 3\%, low
likelihood 3\%, and long inter-beat periods (48 bpm).

x29 QRS-HMM is OK.  Episodes of arrhythmia with frequent premature
beats.  Sometimes every 5th beat is premature.  Solution: Mark this
record as normal and don't fix the derived signals.

2024-07-23 Errors on test data: 3704 21\%.  Not satisfactory given the
complexity of the effort.  Next, I will try to get close to 80\% with
a simple approach.
