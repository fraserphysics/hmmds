\documentclass[usenames, dvipsnames]{beamer}
\setbeamertemplate{navigation symbols}{} %no nav symbols
\usepackage{graphicx,xcolor}
\usepackage{amsmath, amsfonts}% ToDo: compatible w/siammathtime.sty?
\usepackage{amsthm}% Note: Conflicts with newsiambook
\usepackage{xspace}
\usepackage{bm} % Bold Math
\usepackage{rotating} % for sidewaysfigure
\usepackage{afterpage}
\usepackage{booktabs}       % for nicer looking tables
\usepackage{dcolumn}        % decimal point aligned columns
\renewcommand{\th}{^{\text th}}
\newcommand{\field}[1]{\mathbb{#1}}
\newcommand{\INTEGER}{\field{Z}}
\newcommand{\REAL}{\field{R}}
\newcommand{\COMPLEX}{\field{C}}
\newcommand{\EV}{\field{E}}
\newcommand{\y}{\mathbf{y}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\s}{{\bf s}}
\newcommand{\bS}{{\bf S}}
\newcommand{\Y}{{\bf Y}}
\newcommand{\Tsamp}{\tau_s }
\newcommand{\ColorComment}[3]{}
\newcommand{\argmin}{\operatorname*{argmin}}
\newcommand{\argmax}{\operatorname*{argmax}}
\newcommand{\Normal}{{\mathcal{N}}}
\newcommand{\NormalE}[3]{{\mathcal{N}}\left.\left(#1,#2\right)\right|_{#3}}
\newcommand{\transpose}{^\top}
\newcommand{\ceil}[1]{\lceil#1\rceil}
\newcommand{\bceil}[1]{\left\lceil#1\right\rceil}
\newcommand{\floor}[1]{\lfloor#1\rfloor}
\newcommand{\bfloor}[1]{\left\lfloor#1\right\rfloor}
\newcommand{\states}{{\cal S}}
\newcommand{\outputs}{{\cal Y}}
\newcommand{\State}{S}
\newcommand{\Output}{Y}
\newcommand{\parameters}{\theta}
\newcommand{\parametersPrime}{\theta'}% for EM1.gpt

\newcommand{\ti}[2]{{#1}{(#2)}}                  % Time Index
%%% \newcommand{\ts}[3]{{#1}{(t\=#2,\ldots,#3)}}         % Time Sequence
\newcommand{\ts}[3]{#1_{#2}^{#3}}                    % Time Sequence
%%% \newcommand{\ts}[3]{\left\{ #1(l) \right\}_{l=#2}^{#3}}  % Time Sequence
\newcommand{\id}{{\bf I}}
\newcommand{\ie}{i.e.\xspace}
\newcommand{\eg}{e.g.\xspace}
\newcommand{\etal}{et al.\xspace}
\newcommand{\iid}{i.~i.~d.\xspace}
\newcommand{\apost}{\emph{a posteriori}\xspace}
\newcommand{\apri}{\emph{a priori}\xspace}
\newcommand{\plotsize}{\small}
\newcommand{\mlabel}[1]{\label{#1}}
\newcommand{\EMmap}{{\mathcal T}} %

\newcommand{\Green}[1]{{\color{ForestGreen}#1}}
\newcommand{\Red}[1]{{\color{red}#1}}
\newcommand{\Blue}[1]{{\color{blue}#1}}
\newcommand{\given}{~|~}


\title{Data Assimilation and Software for Reliability}

\author{Andrew M.\ Fraser}
\date{2021-5-24}
\institute{SIAM DS21}

\usetheme{default}
\usefonttheme[]{serif}
\begin{document}
\frame{\titlepage}

%\frame{\frametitle{Outline}\tableofcontents}

\section{Book and Goals}

\frame{ \frametitle{Book from MS on Hidden Markov Models at DS01}
  \begin{columns}
    \column{0.5\textwidth}%
    \resizebox{1.0\textwidth}{!}{\includegraphics{book_cover.jpg}}
    \column{0.5\textwidth}%
    Discrete state dynamics
    \begin{equation*}
      P(s[t+1] \given s[t])
    \end{equation*}
    Discrete observations
    \begin{equation*}
      P(y[t] \given s[t])
    \end{equation*}
  \end{columns}
  %\resizebox{0.45\textwidth}{!}{\includegraphics{error.jpg}}
  }

\frame{ \frametitle{Goals for Book and Software}

  \begin{description}
  \item[Reproducible:] Fetch source, type \emph{make}, wait $\sim 30$
    hours, view resulting \emph{main.pdf}.
  \item[Readable Code:] In 2002 I chose Python instead of \emph{C,
      Perl} and or \emph{Octave}.
  \item[Shortcomings:]$~$
    \begin{itemize}
    \item Unreadable \emph{Makefile}.  Love/hate relationship with
      \emph{make}.
    \item I focused on appearance of result at expense of readability.
    \item Code was difficult to read.
    \item \Red{No testing framework.}  % red
    \end{itemize}
  \end{description}
}

\section{Best Practices for Scientific Computing}%
\label{sec:best_practices}

\frame{ \frametitle{Best Practices for Scientific Computing}
  Discovered \emph{Software Carpentry} in 2015.  \emph{Best Practices
    for Scientific Computing} by G. Wilson et al. PLOS 2014 says:
  \begin{itemize}
  \item \Red{Write programs for people}, not computers.  (Tools like
    black and pylint help.)
  \item Let the computer do the work.  (\Blue{Use a build tool.})
  \item Make \Red{incremental changes}.  (Put everything that has been
    created manually in \Blue{version control}.)
  \item \Red{Don't repeat yourself}.  (Every piece of data must have a
    single authoritative representation in the system.)
  \item \Red{Plan for mistakes}.  (Use an off-the-shelf \Blue{unit testing}
    library.)
  \item Optimize software only after it works correctly.
  \item \Red{Document design and purpose}, not mechanics.
  \item \Red{Collaborate}.
  \end{itemize}
}


\frame{ \frametitle{New Text and Software for Book}
  Goals: Follow best practices
  \begin{description}
  \item[Implemented Testing:] Wham! Test of \Red{decoding sequences of
    classes fails}.
  \item[Conceptual Error:] Code assumes classes have Markov property
    like states (more on this later).
  \item[Morals:] Follow best practices.  Listen to doubters.
  \end{description}
}

\frame{ \frametitle{New Text and Software for Book (2)}
  Progress on new code after Los Alamos:\medskip\\
  \begin{description}
  \item[Follow Google Coding Standards:]
  \item[Built in Documentation:]
  \item[Built in Testing:]
  \item[Investigated Class Decoding:] Complexity is
    exponential in length.  Shortcuts I've tried perform badly.
  \end{description}
}


\section{Apnea and Estimating Classes}%
\label{sec:apnea}

\frame{ \frametitle{Estimating Class to Detect Apnea}
  Computers in Cardiology 2000 Challenge: Classify EKG
  \begin{columns}[c]
    \column{0.45\textwidth}%
    \begin{center}
    Normal\\
    \resizebox{\textwidth}{!}{\includegraphics{a03erN.pdf}}
    \end{center}
    \column{0.45\textwidth}%
    \begin{center}
      Apnea\\
      \resizebox{\textwidth}{!}{\includegraphics{a03erA.pdf}}
    \end{center}
  \end{columns}
}

\frame{ \frametitle{Multiple Apnea Models}
  Happy sleepers are all alike; every unhappy sleeper is unhappy in
  its own way.
  \begin{center}
    \resizebox{0.6\textwidth}{!}{\input{happy.pdf_t}}
  \end{center}
  \vspace{-1cm}
\begin{columns}[c]
  \column{0.5\textwidth}%
  \begin{center}
    Mode $N_0$\\
  \resizebox{0.3\textwidth}{!}{\input{normal_0.pdf_t}}
  \end{center}
  %\column{0.2\textwidth}%
  \column{0.5\textwidth}%
  \begin{center}
  Mode $A_w$\\
  \resizebox{0.6\textwidth}{!}{\input{apnea_w.pdf_t}}
  \end{center}
\end{columns}

}

\frame{ \frametitle{Estimating Sequences}
  \begin{description}
  \item[Viterbi Decoding for States:] Computation is \Red{linear in $T$.}
    \begin{equation*}
      \hat s[0:T] \equiv \argmax_{\text{state}[0:T]} P\left(
        \text{state}[0:T] \given \text{heart rate}[0:T] \right)
    \end{equation*}
  \item[Class Sequence from State Sequence:] More apnea modes
    $\rightarrow$ \Red{Less apnea estimated.}
    \begin{equation*}
      \hat c[t] = C\left( \hat s[t] \right)
    \end{equation*}
    Probability gets spread over many modes.
  \item[Max A-posteriori Prob Class Sequence:]  \Red{Exponential in $T$.}
    \begin{equation*}
      \hat c[0:T] \equiv \argmax_{\text{class}[0:T]} P\left(
        \text{class}[0:T] \given  \text{heart rate}[0:T] \right)
    \end{equation*}
  \item[Sequence of MAP Classes:] Linear in $T$, but can yield
    \Red{impossible sequences.}
    \begin{equation*}
      \hat c[t] = \max_{\text{class}}
        P\left( \text{class}[t] \given \text{heart rate}[0:T] \right)
      \end{equation*}
      \Red{Appropriate for 2-class apnea problem.}
  \end{description}
}

\frame{ \frametitle{Graphical Representation of Conditional Independence}
  Blocking out \Red{$s_{t+1}$ separates the past from the future}, but
  blocking out $c_{t+1}$ doesn't.
  \begin{center}
    \resizebox{0.75\columnwidth}{!}{\input{class_net.pdf_t}}
  \end{center}
  A bad subsequence of classes may later become a portion of the
  \emph{best} class sequence.  The number of subsequences to calculate
  and store is exponential in length $T$.  }

\frame{ \frametitle{Conclusions}
  \begin{itemize}
  \item Structure work and code for clarity.
  \item Collaborate and seek peer review.
  \item Consider advice about good practices.
  \item Focus on objectives before algorithms.
  \item Estimating class sequences could be important and interesting.
  \end{itemize}
}

\end{document}

1.  Hello, I'm Andy Fraser.

Since I quit Los Alamos National Laboratory last fall, one of my goals
has been revising the text and code for my book ``Hidden Markov Models
and Dynamical Systems''.

Today, I'll talk about progress towards that goal and some things I've
learned.



2. Here's a picture of the cover of the book.

The big lesson for today is that if you put errors in your book, you
will have to put green stickers on the covers.

The book arose from a couple of mini-symposiums that Kevin Vixie
organized for this meeting 20 years ago in 2001.

When people at that meeting asked where to learn about the subject,
the speakers couldn't think of a good resource.

The SIAM book acquisition manager at the time was Linda Theil, and she
was in the audience.  She suggested that the speakers write a book.

Hidden Markov Models represent the simplest kind of dynamical system
that you can use for data assimilation.

Once you learn the fundamental ideas of data assimilation using Hidden
Markov Models, you have a context for other models and techniques like
Kalman filtering or particle filtering.

In a Hidden Markov Model the states and observations are discrete.
These discrete conditional probability distributions define the model.

A group of the speakers agreed to write a book.

Now I ended up being the sole author, but my favorite chapter of the
book is the one on performance bounds, and it draws heavily from Kevin
Vixie's dissertation.  And I also got a lot of help organizing the
code and figures for the book from a man named Karl Hegbloom.

There are some errors in the book because I didn't read the galley
proofs carefully enough.

But the error that earned the green stickers was a conceptual error
that I made in an algorithm that estimates classification sequences
based on sequences of observations.

I used that algorithm in the last chapter of the book on apnea
classification.

I will talk more about that in a few minutes.

Hidden Markov Models are also graphical models.  I'll explain the
error that I made in terms of a picture of the graph that represents
conditional independence relations.


3. When I started working on the book, I wanted the project to be open
source and all of the examples to be completely reproducible.

I was somewhat successful.  I could check out the entire project
from version control, type make, and after more than a day of
grinding, my workstation would produce a pdf of the entire book.

Others found it possible, if a little difficult, to configure their
workstations to do the same thing and get the same result.

I also wanted the code to be easy to read.  I am still pleased with my
choice of Python.

After 20 years scientific computing with Python has improved and so
have my ideas about readability.

The shortcomings of the original code included:

* The Makefile was hard to read.  I wish it were easier or even
possible to write readable makefiles.

* I focused too much on the appearance of the book at the expense of
readable code.

* And I didn't write unit tests.  That would have saved me from the
green stickers.


4. In 2005 when I started working at Los Alamos, I was surprised to
find many projects were not using modern software tools.

When I found that instead of using a version control tool, scientists
were emailing versions of code back and forth.  I began thinking about
how to teach modern software practices.

To figure out a curriculum for lessons, I signed up for Software
Carpentry Instructor Training at the 2016 SciPy conference in Austin.

There I learned that Greg Wilson started Software Carpentry based on
lessons that he and a colleague gave at Los Alamos between 1998 and
2002.

I also read the paper Best Practices for Scientific Computing by
Wilson et al. which was derived from their experience teaching
Software Carpentry.

The paper hits all the right points with the right emphasis.  Here are
the 8 practices listed in the paper.  (I've used red to emphasize key
goals, and I've used blue to emphasize key tools)

I especially like these practices.

* Write programs for people to read.  Including yourself after a few
months or years.

* Use a build tool.  After trying a few others, I'm still using make.

* Use a version control tool. I've used several.  I use git now.

* Do unit testing.  I've learned that unit testing is really useful.

I ended up organizing a team of instructors at Los Alamos who now
offer Software Carpentry Workshops a few times every year.


5.  The experience has helped me formulate goals for new versions of
the text and software for the book.

As a first step, I implemented some unit tests and WHAM, I discovered
my idea for estimating sequences of classifications was wrong, wrong,
wrong.


6. I didn't make a lot of progress rewriting the code part time while
going to The Lab every day, but since I left Los Alamos, I've made a
good start.

I've set up a workstation to run NIXOS which provides tightly
controlled build environments.  Like CONDA but stricter

My new code follows the Google coding standards.

I'm using SPHINX for documentation.

And I'm using Python's built in unit testing framework.

I've written all of the basic Hidden Markov Model algorithms and
enough code specifically for analyzing electro-cardiograms to look at
the apnea classification example in the last chapter of the book.


7. The last chapter of the book is based on data from a contest in
2000.  The goal is to use only electro-cardiogram data to determine
when patients were having episodes of obstructive sleep apnea.

Apnea patients stop breathing roughly every 45 seconds and then wake
up just enough to begin breathing again and keep living.

Since they don't wake up enough to remember the events, they think
they've slept 8 hours and don't know why they're so tired in the
morning.

Here's some data from normal sleep.  The respiration is regular.  The
Oxygen saturation is constant and above 90\%.  You can see that the
heart rate is fairly constant because the spikes in the
electro-cardiogram are evenly spaced.

And here's some data from an apnea episode.  The respiration is
blocked for about 35 seconds followed by 10 seconds of gasping.

The measured Oxygen saturation follows a cycle that ranges from above
90\% to below 60\%.  And the heart rate fluctuates following the
occlusion cycle.

For different patients, the kinds of patterns that occur in the apnea
episodes may differ.


8. To accommodate those differences, I made a collection of models for
different apnea modes.

In this figure the big boxes represent classes and each small box
represents a model for a mode.  Each of those mode models can contain
many states.

With separate models for many apnea modes some estimation algorithms
classify an interval as normal because the normal mode has higher
probability than the probability of any single apnea mode.

That's even though the total probability assigned to the all of the
states in the apnea class combined is larger than the probability
assigned to the all of the states in the normal class.


9. I've tried several different approaches for using heart rate data
to estimate class.

* The classic algorithm for calculating the best sequence of states
given a sequence of observations is called the Viterbi Algorithm.

It's named for Andrew Viterbi, the co-founder of Linkabit and Qualcomm.

The Viterbi algorithm calculates the best entire sequence of states.
Where best is Maximum A-posteriori Probability.

* To get an estimated sequence of classes, you can just look up to
find which class contains each state from the Viterbi algorithm.

With that approach, having lots of models of apnea modes leads to
estimated sequences of classes that are all normal.

* Imitating the Viterbi algorithm to seek the best entire sequence of
classes seems like a good idea, but the computational expense is
exponential in the length of the data.

My old code claimed to produce exactly such an estimate with a linear
computational expense.  It's too bad that it's not possible.

* Alternatively, at each time you can calculate the best guess of the
class given all of the data.

In general that approach can give you a sequence of classifications
that has class transitions that are impossible according to the model.

But for the two class apnea classification problem, no class
transitions are impossible, and this approach is appropriate.


10. This graphical representation of the model shows the conditional
independence relations of the variables.

Notice that removing the node for the state at time t+1 breaks the
graph into 4 disconnected pieces.

The collections of variables in each of those pieces are conditionally
independent of each other given the value of the state at time t+1.

Knowing the value of the state at time t+1 makes the values of earlier
variables irrelevant for forecasting future values.

That conditional independence is called the Markov property.  It is
essential for all of the techniques for working with Hidden Markov Models.

On the other hand removing the class at time t+1 from the graph
doesn't provide a similar partition.

So knowing the class at time t+1 does not make the values of future
variables conditionally independent of past values.

And what appears to be a bad subsequence of past classes may, in view
of later data, become a portion of the best class sequence.  So you
need to keep track of a lot of subsequences

In fact the number of subsequences you need to calculate and store is
exponential in total sequence length.


11. Let me conclude with these lessons learned:

  * Structure your work and code for clarity.
  * Collaborate and seek peer review.
  * Consider advice about good practices.
  * Focus on objectives before algorithms.
  * And further work on estimating class sequences could be important
    and interesting.

%%% Local Variables:
%%% eval: (TeX-PDF-mode)
%%% End:
