%%%
%%% "hmmdsbook" is a subclass of "newsiambook", unless you use the
%%% 'ltxbook' option, in which case it is a subclass of the standard
%%% LaTeX "book".
%%%
%%% Note: The 'nottoc' option should be passed if we do not want the
%%%       TOC to appear in the TOC.  It will be passed along to
%%%       tocbibind.sty by hmmdsbook.cls.
%%%

%\documentclass[]{hmmdsbook} % For old siam style
\documentclass[ltxbook,showlabels, commentsvisible]{hmmdsbook}
%\documentclass[ltxbook, commentsvisible]{hmmdsbook}
%\documentclass[ltxbook]{hmmdsbook}

%%% Note: The following settings will cause many overfull hbox false alarms:
% \documentclass[dvips,prelim,showlabels]{hmmdsbook}
% %%% Show index entries in margin
% \proofmodetrue

%%% I'm putting this in here for our own runs, since I think that it
%%% provides a reasonable facimile of the font metrics that the final
%%% document will have, at least for the plot labels.  This should
%%% probably be removed or commented off before the final run.
%%%
\usepackage{type1cm}
\usepackage[]{nomencl}

\renewcommand{\plotsize}{%
  \fontsize{8.5}{8.5}%
  \selectfont%
}


%%% I found that the crop.sty included with TeXlive works a lot better
%%% than the one included with the SIAM book macro package.  I've
%%% moved theirs aside so the TeXlive version is used.
%%%
\usepackage[letter,center]{crop}
\crop

\usepackage{fancybox,framed}
\usepackage{rotating}

\author{\copyright Andrew M. Fraser}
\title{Hidden Markov Models and Dynamical Systems}


\begin{document}
\frontmatter
\maketitle

\chapter*{Preface}
\addcontentsline{toc}{chapter}{Preface}
\label{chap:preface}

This book arose from a pair of symposia on hidden Markov models that
Kevin Vixie organized at the 2001 SIAM Dynamical Systems meeting.  At
the end of the first symposium someone asked for a simple reference
that explains the basic ideas and algorithms for applying HMMs.  We
were stumped\footnote{In subsequent years several good references that
  explain the basic ideas of HMMs have appeared, \eg, \ToDo{Find
    some}}.  A group of the participants suggested writing this book
to answer the question.  Two years later, Fraser alone delivered a
proposal for the book that included about two chapters to SIAM at the
2003 Dynamical Systems meeting.  Sometime after SIAM published the
first edition in 2008, we (Fraser alone again) began working on
updating the software we used to generate the book.  In an effort to
use better software practices, we wrote tests, and one of those tests
revealed a conceptual error in the last chapter of the book.  In the
current edition Chapter~\ref{chap:apnea} is a thorough revision that
addresses the same tasks with a simpler approach.

The book is intended for readers who have backgrounds and interests
typical of those who attend the SIAM Dynamical Systems meetings.  In
particular, our view is that HMMs are discrete state discrete time
stochastic dynamical systems, and that they are often used to
approximate dynamical systems with continuous state spaces operating
in continuous time.  Thus, by using familiar analogies, it should be
easy to explain HMMs to readers who have studied dynamical systems.

The basic techniques were well developed in the 1970's for work on
speech and language processing.  Many in speech research in the 1980's
through the end of the century learned about the techniques at a
symposium held at the Institute for Defense Analysis \index{Institute
  for Defense Analysis} in Princeton.  A small number of proceedings
of that symposium \cite{ida80} were printed, and the volume was called
\emph{the blue book} by workers in the field.  We were not part of
that community, but we have a copy of the blue book.  It explains the
basic algorithms and illustrates them with simple concrete examples.
We hope our book is as simple, useful, and clear.

In chapters \ref{chap:variants}, \ref{chap:continuous} and
\ref{chap:particles} we extend ideas from HMMs with discrete states and
observations to more general states and observations.  The relevant
literature of those more general systems is called \emph{data
  assimilation}.  Because the discrete character of the states and
observations of basic HMMs makes the ideas and algorithms for them
easier to explain and understand than more general data assimilation
techniques, we recommend studying HMMs before working on applications
that require more those general data assimilation tools.

Although there are now other books and papers that are about HMMs
exclusively or in part, we believe that the present volume is unique in
that:
\begin{description}
\item[It is introductory] Sophomore or Junior study in engineering,
  mathematics, or science provides the prerequisites for most of the
  book.  The exceptions are ideas from dynamical systems and
  information theory.  In particular, we use the Gibbs inequality
  (Eqn.~\eqref{eq:GibbsIE}) in developing the EM algorithm in Chapter
  \ref{chap:algorithms}.  Although Chapter \ref{chap:toys} \emph{Toy
    Problems and Performance Bounds} deals with Lyapunov exponents and
  entropies, it is not a prerequisite for any other chapter.
\item[Algorithms are explained and justified] We present enough of the
  theory behind the basic algorithms in Chapter \ref{chap:algorithms}
  so that a reader can use it as a guide to developing her own
  variants.
\item[We provide Python and data for the algorithms and examples] You
  can fetch the code and data and build the book yourself.  You can
  examine the source code to resolve questions about how figures were
  made, and you can build on the software to try out your own ideas
  for variations.
\item[It uses analogies to dynamical systems] For example, I
  demonstrate the HMM training algorithm by applying it to data
  derived from the Lorenz system.  The result, as
  Fig.~\ref{fig:Statesintro} illustrates, is that the algorithm
  estimates a discrete state generating mechanism that is an
  approximation to the state space of the original Lorenz system.
\item[We illustrate with practical examples] In Chapter
  \ref{chap:apnea} I present an application to experimental
  measurements of electrocardiograms (ECGs).  \ToDo{I want a practical
    application of particle filtering in Chapter \ref{chap:particles}}
\end{description}

\ToDo{ Point to review papers and material (I've collected some of the
  kind of stuff I have in mind at
  \url{http://fraserphysics.com/~andy/HMMs/} ).  Perhaps organize this
  stuff under the following topics:
  \begin{itemize}
  \item MCMC annealing and optimization
  \item Other applications
  \item Other papers books and web sites
  \item Bayes nets
  \end{itemize}
}

\subsection*{Source Software, Data and Text}
\label{preface_software}

The software, data and text files we used to build the book are freely
available.  We've written a Python package named \emph{hmm} that
provides implementations of the general algorithms described in the
book.  It is available from https://gitlab.com/fraserphysics1/hmm.  At
https://gitlab.com/fraserphysics/hmmds we provide additional code,
text and data for building the book.  After fetching and installing
both packages one can issue ``make book'' from a command line to
compile the software, run the numerical experiments, make the figures
and format the entire book.  \ToDo{Once I get that fixed, give
  detailed instructions for different environments.}

Since our fortunate decision to use Python for the first edition of
this book the continuing evolution of Python language and ecosystem
has made writing code easier and the resulting code is easier to read.
Although algorithms are given in pseudo code in the text, we provide a
working implementation of each of the algorithms described in the
text.

\subsection*{Notation}
\label{preface_notation}

In general we introduce notation as it gets used in the text and
collect it in a section at the end of the book on page~\pageref{sec:notation}.  However to avoid confusion, we now note our
use of Python notation for sequences, namely
\begin{equation*}
  \ts{x}{0}{T} \equiv \left[ x[0], x[1], \ldots, x[T-1] \right].
\end{equation*}
There are a few different notation conventions for probabilities and
stochastic processes each of which has drawbacks.  We describe the
conventions we've chosen in the notation section starting on page
\pageref{sec:notation}.

\section*{Acknowledgments}
\addcontentsline{toc}{section}{Acknowledgments}%
\label{sec:ack}
\longpage%%% Maybe not required for MathTime... look for partial
         %%% paragraph on page by itself before TOC begins.

In writing this book, we have used much that we learned from colleagues
in Portland.  In particular, Todd Leen kept us informed about
developments in the machine learning community.  We've relied on the
review of the ergodic theory of dynamical systems in Kevin Vixie's
dissertation\cite{vixie02} for Chapter~\ref{chap:toys} of this book.
Shari Matzner and Gerardo Lafferriere helped us understand the
convergence properties of the EM algorithm.  Also the following
colleagues have given us constructive comments on drafts of the book:
% 
Patrick Campbell, %
Ralf Juengling, %
Shari Matzner, %
Cosma Shalizi, %
and %
Rudolph Van der Merwe. %

We thank the following people and organizations for providing the data
that we used for examples:
\begin{description}
\item[Carl Otto Weiss] for providing Tang's\cite{Tang92} laser data
\item[PhysioNet\cite{PhysioNet}] For providing Penzel's\cite{Penzel02}
  ECG data and the WFDB python package\cite{WFDB-python} for accessing
  that data from python.
\item[Project Gutenberg] For digitizing and distributing \emph{A
    Book of Prefaces},\\ by H.~L.~Mencken
%%% I hate to put a line break in there like that, but it was hanging
%%% out into the margin without it. -- karlheg
\end{description}

We were fortunate to meet the late Karl Hegbloom in Portland.  Karl
contributed to several free software projects and he helped us with
the figures and software for the fist edition.  In addition to
personal help with software, we relied on free software written by too
many contributors to list.  We used the NixOS distribution of Linux to
organize the software.

For writing the first edition, we acknowledge Portland State University
for support of a sabbatical leave
%


\tableofcontents
%%%\addcontentsline{toc}{chapter}{Table of Contents}%
%%% Enable these to make it easier to find them during editing phase
% \listoffigures% Not unless required by SIAM
% \listoftables% Not unless required by SIAM
%%% \listofalgorithms%% There are none, since at this point they are done as figures.

\mainmatter
\include{introduction}
\include{algorithms}
\include{variants}
\include{continuous}
\include{toys}
\include{apnea}
\include{particles}
\include{appendix}


\backmatter
% Begin ``Notation'' material
\renewcommand{\nomname}{Notation}%
\renewcommand{\pagedeclaration}[1]{\ (page #1)}%
\setlength{\nomlabelwidth}{2.0cm}%
\renewcommand{\nompreamble}{%
  \label{sec:notation} % For referencing page
  Throughout the book we write about random variables and stochastic
  processes.  We have tried to select notation that is as simple as
  possible without being ambiguous.  To illustrate the challenge
  suppose that we have been talking about gambling and the weather
  and that we say ``the probability of 5 is 0.16''.  Among the many
  things that we could mean are the following:
  \begin{equation}
    \label{eq:dice}
    \mathbf{Prob}(\text{top face of die}=5) = 0.16
  \end{equation}
  \begin{equation}
    \label{eq:rain}
    \lim_{\epsilon \rightarrow 0} \frac{\mathbf{Prob}(5 < \text{total
        rainfall today in millimeters}<5+\epsilon)}{\epsilon} = 0.16
  \end{equation}
  As a less cumbersome notation we prefer $P_D(5)=0.16$ or
  $P_R(5)=0.16$, where $P$ denotes either a probability mass function
  or a probability density function.  We only use a subscript when it
  is necessary to specify which function we mean.

  In general we use the following conventions:
  \begin{symbdescription}
  \item[$X$] Upper case indicates a random variable.  Although the
    notation does not suggest it, the notion of a random variable
    includes a set of possible values or outcomes and a probability
    distribution for those values.
  \item[$x$] Lower case indicates an \emph{outcome} or value of a
    random variable.
  \item[${\cal X}$] Occasionally we use a calligraphic font to
    indicate the \emph{alphabet} or set of all possible values of a
    random variable $X$.
  \item[$\ts{X}{0}{T}$] A stochastic process indexed by the sequence
    $[0,1,2,\ldots,T-1]$, \ie,\\
    $\ti{X}{0},\ti{X}{1}, \ldots, \ti{X}{T-1}$.
  \item[$\ts{x}{0}{T}$] A particular possible outcome of the stochastic
    process $\ts{X}{0}{T}$.
  \item[$\ti{X}{t}$] The random variable that results from picking a
    single component of a stochastic process.
  \item[$P(x)$] The probability (or density) that a random variable
    will have the particular value $x$.  We do not put a subscript on
    $P$ when the context permits us to drop it without ambiguity.
  \item[$P_{\ti{X}{t}} \left(x \right)$] The probability that the
    value of $\ti{X}{t}$ is $x$.
  \item[$P_{\ti{X}{t+1}\given \ti{X}{t}} \left(x_a\given x_b \right)$] The
    conditional probability that the value of $\ti{X}{t+1}$ is $x_a$
    given that the value of $\ti{X}{t}$ is $x_b$.
  \item[$\mu(\beta)$] The probability that an outcome is in the
    \emph{set} $\beta$.  We occasionally use this notation from
    measure theory in Chapter~\ref{chap:toys}.
  \item[$P \left(x\given \theta \right)$] Rather than a subscript, we
    occasionally use a conditioning variable to specify one of many
    possible probability distributions.
 \end{symbdescription}

  \section*{Symbols}
  The following symbols usually have the meanings described below:
}
\cleardoublepage%
\markboth{\nomname}{\nomname}%% Required to get correct page heading labels.
\printnomenclature
% End ``Notation'' material

\chapter*{Bibliography}
\addcontentsline{toc}{chapter}{Bibliography}%

\bibliography{hmmds}% \bibliographystyle is set in class definition.

%%% \printindex prints the label ``Index'' already, and also seems to
%%% do a \clearpage first.
%%%
\addcontentsline{toc}{chapter}{Index}%
\printindex[default][Page numbers set in \textbf{bold} type indicate
that the entry refers to a definition.]

\end{document}

ToDo:

Maybe work to get better parts of speech

z-axis of fig:LaserLogLike y_1^250 -> y[0:250]

fig:GaussMix \theta(1) -> \theta[0]

Shrink text in fig:forward

Check color highlighting around eq:gamma

Add to Notation \alpha_n etc in fig:train around page 35

3-d rotation of cover fig in qt-5 viewer.

Things to mention in Conclusion chapter:

* Graphical Models

References:

https://arxiv.org/pdf/1105.1476 EM algorithm and variants: an informal
  tutorial See Section 2.5. EM as a fix point algorithm and local
  convergence

https://physionet.org/content/wfdb-python/4.1.0/ Waveform Database
  Software Package (WFDB) for Python

https://github.com/fraserphysics/hmmds/tree/master/doc/source

https://web.stanford.edu/class/ee364b/latex_templates/template_notes.pdf
  LATEX Style Guide for EE 364B and
  https://news.ycombinator.com/item?id=41756286

%%%---------------
%%% Local Variables:
%%% eval: (load-file "hmmkeys.el")
%%% mode: LaTeX
%%% End:
