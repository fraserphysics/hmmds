\input{apnea_values.tex}
\chapter{Obstructive Sleep Apnea}
\label{chap:apnea}

The challenge for the \emph{Computers in Cardiology} %
\index{Computers in Cardiology 2000 (CINC2000)}%
meeting in 2000 (CINC2000) %
\index{CINC2000 (Computers in Cardiology 2000)}%
was to identify \emph{obstructive sleep apnea} on the basis of
electrocardiograms alone.  Our colleague James \index*{McNames} at
Portland State University, with some support from us, won the prize
for the best minute by minute analysis of data.  We prepared our first
entry using HMMs, and McNames prepared the subsequent (and winning)
entries by hand using spectrograms\footnote{A spectrogram is display
  of power in Fourier spectral bands as a function of time.  The
  x-axis is time, the y-axis is frequency, and the image intensity at
  point $(t,f)$ is the power in that frequency estimated in a window
  centered at that time.} to visualize the data.  \index{spectrogram}

In preparing the first entry, we noticed that apnea produced many
different characteristics in heart rate time series while the
characteristics in non-apnea times were more similar to each other.  We
used a larger number of states in HMMs to model the various
characteristics of apnea and a smaller number of states to model the
more consistent normal times.  However using the maximum a posteriori
probability (MAP) estimate sequence of states from the Viterbi
algorithm, ie,
\begin{equation*}
  \ts{\hat s}{0}{T} \equiv
  \argmax_{\ts{s}{0}{T}}P\left(\ts{s}{0}{T} \given  \ts{y}{0}{T} \right)
\end{equation*}
to estimate the sequence of classifications $\hat {\ts{c}{0}{T}}$ with
\begin{equation*}
  \ti{\hat c}{t} \equiv c: \ti{\hat s}{t} \in c
\end{equation*}
produced obvious errors.  We found that during many periods of apnea
the probability of being in each of the apnea states was lower than
the probability of being in the normal state with the highest
probability while the sum of the probabilities of apnea states was
larger than the sum over normal states.  Our effort to model the
diversity of apnea characteristics led to decoded sequences that were
normal too often.

We tried to address the issue by estimating the MAP sequence of classes
rather than the MAP sequence of states, ie,
\begin{equation}
  \label{eq:decode_class}
  \ts{\hat c}{0}{T} \equiv
  \argmax_{\ts{c}{0}{T}}P\left(\ts{c}{0}{T} \given  \ts{y}{0}{T}\right).
\end{equation}
While the computational complexity of Viterbi algorithm which finds
the MAP sequence of states is linear in $T$, the length of the
sequence of observations, the computational complexity required to
solve \eqref{eq:decode_class} is exponential\footnote{In the first
  edition of this book, we presented an algorithm that we claimed solved
  \eqref{eq:decode_class} in linear complexity.  In some cases that
  algorithm yields plausible but not necessarily correct results and
  in others it simply crashes.} in $T$.

In this chapter, we will first describe the CINC2000 challenge and how
McNames addressed it.  Then we will address the challenge using HMMs.

\section{The  Challenge and the Data}
\label{sec:challenge}

The PhysioNet
website\footnote{\url{http://www.physionet.org/challenge/2000}}
announced the challenge and described the data as follows:

{\it % italic to signify quote
  {\rm \textbf{Introduction:}}
  %
  Obstructive sleep apnea (intermittent cessation of breathing) is a
  common problem with major health implications, ranging from
  excessive daytime drowsiness to serious cardiac arrhythmias.
  Obstructive sleep apnea is associated with increased risks of high
  blood pressure, myocardial infarction, and stroke, and with
  increased mortality rates. Standard methods for detecting and
  quantifying sleep apnea are based on respiration monitoring, which
  often disturbs or interferes with sleep and is generally expensive.
  A number of studies during the past 15 years have hinted at the
  possibility of detecting sleep apnea using features of the
  electrocardiogram. Such approaches are minimally intrusive,
  inexpensive, and may be particularly well-suited for screening. The
  major obstacle to use of such methods is that careful quantitative
  comparisons of their accuracy against that of conventional
  techniques for apnea detection have not been published.

  We therefore offer a challenge to the biomedical research community:
  demonstrate the efficacy of ECG-based methods for apnea detection
  using a large, well-characterized, and representative set of data.
  The goal of the contest is to stimulate effort and advance the state
  of the art in this clinically significant problem, and to foster
  both friendly competition and wide-ranging collaborations. We will
  award prizes of US\$500 to the most successful entrant in each of two
  events.

  {\rm \textbf{Data for development and evaluation:}}
  %
  Data for this contest have kindly been provided by Dr. Thomas
  Penzel \index{Penzel, Dr.\ Thomas}  of Philipps-University, Marburg,
  Germany [available on the website].

  The data to be used in the contest are divided into a learning set
  and a test set of equal size. Each set consists of 35 recordings,
  containing a single ECG signal digitized at 100 Hz with 12-bit
  resolution, continuously for approximately 8 hours (individual
  recordings vary in length from slightly less than 7 hours to nearly
  10 hours). Each recording includes a set of reference annotations,
  one for each minute of the recording, that indicate the presence or
  absence of apnea during that minute. These reference annotations
  were made by human experts on the basis of simultaneously recorded
  respiration signals. Note that the reference annotations for the
  test set will not be made available until the conclusion of the
  contest. Eight of the recordings in the learning set include three
  respiration signals (oronasal airflow measured using nasal
  thermistors, and chest and abdominal respiratory effort measured
  using inductive plethysmography) each digitized at 20 Hz, and an
  oxygen saturation signal digitized at 1 Hz. These additional signals
  can be used as reference material to understand how the apnea
  annotations were made, and to study the relationships between the
  respiration and ECG signals. [...]

  {\rm \textbf{Data classes:}}
  %
  For the purposes of this challenge, based on these varied criteria,
  we have defined three classes of recordings:
  \begin{description}
  \item[Class A (Apnea):] These meet all criteria. Recordings in class
    A contain at least one hour with an apnea index of 10 or more, and
    at least 100 minutes with apnea during the recording. The learning
    and test sets each contain 20 class A recordings.
  \item[Class B (Borderline):] These meet some but not all of the
    criteria. Recordings in class B contain at least one hour with an
    apnea index of 5 or more, and between 5 and 99 minutes with apnea
    during the recording. The learning and test sets each contain 5
    class B recordings.
  \item[Class C (Control):] These meet none of the criteria, and may
    be considered normal. Recordings in class C contain fewer than 5
    minutes with apnea during the recording. The learning and test
    sets each contain 10 class C recordings.
  \end{description}

  {\rm \textbf{Events and scoring:}}
  %
  Each entrant may compete in one or both of the following events:
  \begin{description}
  \item[1. Apnea screening:] In this event, your task is to design
    software that can classify the 35 test set recordings into class A
    (apnea) and class C (control or normal) groups, using the ECG
    signal to determine if significant sleep apnea is present.  [...]
  \item[2. Quantitative assessment of apnea:] In this event, your
    software must generate a minute-by-minute annotation file for each
    recording, in the same format as those provided with the learning
    set, using the ECG signal to determine when sleep apnea occurs.
    Your annotations will be compared with a set of reference
    annotations to determine your score. Each annotation that matches
    a reference annotation earns one point; thus the highest possible
    score for this event will be approximately 16800 (480 annotations
    in each of 35 records). It is important to understand that scores
    approaching the maximum are very unlikely, since apnea assessment
    can be very difficult even for human experts. Nevertheless, the
    scores can be expected to provide a reasonable ranking of the
    ability of the respective algorithms to mimic the decisions made
    by human experts.
  \end{description}
}%% end \it for quote


\subsection{The Data}
\label{sec:data}

Briefly, one can fetch the following records from PhysioNet:
\begin{description}
\item[a01-a20:] The \emph{a records} from individuals that display
  \emph{apnea}
\item[b01-b05:] The \emph{b records}\footnote{The amplitude of the
    \emph{b05} record varies dramatically over different segments of
    time.  We found it unusable and discarded it entirely.} from
  individuals diagnosed as \emph{borderline}
\item[c01-c10:] The \emph{c records} from \emph{control} or normal
  individuals
\item[a01er-a04er and b01er and c01er:] Identical to \emph{a01-a04}
  and \emph{b01} and \emph{c01} except augmented with respiration and
  $SpO_2$ (percent of arterial hemoglobin saturated with oxygen)
  \nomenclature[rSpO]{$SpO_2$}{Percent of arterial hemoglobin
    saturated with oxygen.} signals
\item[summary\_of\_training:] Expert classifications of each minute in
  the $a$, $b$, and $c$ records
\item[x01-x35:] The test set\footnote{The records \emph{x33} and
    \emph{x34} are so similar that we suspect they are simultaneous
    recordings from different ECG leads.  We did not explicitly exploit
  the similarity in our analysis.}; records without classification
\end{description}

Using data visualization tools\footnote{One can use the script
  hmmds/applications/apnea/explore.py from the software we used to
  create this book to explore aspects of the data.} one can see
striking oscillations in the apnea time series.  The patients stop
breathing for tens of seconds, gasp a few breaths, and stop again.
Each cycle takes about 45 seconds and can go on for most of the night.
We've plotted two periods of such an oscillation from record \emph{a03}
in Fig.~\ref{fig:a03erA}.  The reference or \emph{expert}
classifications provided with the data indicate these are the last
oscillations in the first apnea episode of the night.  Over the entire
night's record of 8 hours and 39 minutes, the patient had apnea for a
total of 4 hours and five minutes in 11 separate episodes.  In that
time, more than once a minute, he was waking up enough to start
breathing.  Ten and a half minutes after the end of the first apnea
episode, the record, as plotted in Fig.~\ref{fig:a03erN}, looks
normal.

\begin{figure}
  \centering{\resizebox{\textwidth}{!}{\includegraphics{a03erA.pdf}}
  }
  \caption[A segment of record a03]%
  {A segment of record a03.  Two cycles of a large apnea induced
    oscillation in $SpO_2$ are drawn in the lower plot.  The middle
    plot is the oronasal airflow signal, and the upper plot is the
    ECG.  The time axis is marked in \emph{hours:minutes:seconds}.
    Notice the increased heart rate just after 0:58:00 and just before
    0:59:00.}
  \label{fig:a03erA}
\end{figure}

\begin{figure}
  \centering{\resizebox{\textwidth}{!}{\includegraphics{a03erN.pdf}}
  }
  \caption[A segment of record a03]%
  {A segment of record a03 taken during a period of normal
    respiration.  The signals are the same as in Fig.~6.1.}
  \label{fig:a03erN}
\end{figure}

In plots like Fig.~\ref{fig:a03erA}, the heart rate visibly increases
at the end of the gasping phase and then decreases during the phase of
interrupted respiration.  That heart rate oscillation is the key we
used in our initial attempts to classify periods of apnea.  In
Fig.~\ref{fig:a03erHR}, we've plotted both a heart rate derived from
the ECG \nomenclature[rECG]{ECG}{Electrocardiogram} and the $SpO_2$
signal.  The oscillations in the signals track each other, and the
expert classifies only the region of large heart rate oscillation as
apnea.
\begin{figure}
  \centering{\resizebox{\textwidth}{!}{\includegraphics{a03HR.pdf}}
  }
  \caption[A segment of record 03 at the end of an episode of apnea]%
  {A segment of record 03 at the end of an episode of apnea with
    indications in both the $SpO_2$ signal and the heart rate
    (\emph{HR}) signal.  The expert marked the time before 1:00 as
    apnea and the time afterwards as normal.}
  \label{fig:a03erHR}
\end{figure}

\section{Using Information from Experts to Train}

We first considered the CINC2000 challenge when Andreas Rechtsteiner
told us that he was going to use it as a final project for a class
that Professor McNames was teaching.  We suggested that Rechtsteiner
try a two state HMM with autoregressive observation models, \ie, a
model like those described in section~\ref{sec:ARVGaussian} but with
scalar observations.

To use the expert classification information in training, Rechtsteiner
found that one can just modify the observation model.  The technique
is not limited to HMMs with only two states or two classes; it applies
to an arbitrary number of classes and to arbitrary numbers of states
associated with each class.  At each time $t$, let $\ti{c}{t}$ denote
classification information from an expert about which states are
possible.  Specifically $\ti{c}{t}$ is a vector that indicates that
some states are possible and that the others are impossible.  One
simply replaces $P_{\ti{Y}{t}\given \ti{S}{t},\ts{Y}{0}{t}}$ with
$P_{\ti{Y}{t},\ti{C}{t}\given \ti{S}{t},\ts{Y}{0}{t}}$ wherever it occurs
in the Baum-Welch algorithm.  The modification forces the system into
states associated with the right class during training.

While the performance of Rechtsteiner's two state model was not
memorable, models with more states were promising.

\subsection{The Excellent Eye of Professor McNames}
\label{sec:mcnames}

To diagnose the errors, McNames printed spectrograms of every record.
As he reported in \cite{Mcnames2000ObstructiveSA}, the first step in
McNames analysis was implementing his own QRS\footnote{The ECG of a
  typical heart beat has five distinctive features or \emph{waves}
  unimaginatively called \emph{P,Q,R,S}, and \emph{T}.  The largest
  feature is the spike called \emph{R}.  We have labeled the
  features in Figure~\ref{fig:constant_a03}. } detection algorithm.
Then he derived spectrograms from the resulting QRS analyses.
Although the spectrograms discarded phase information that we hoped
was important, they clearly indicated the oscillations that we had
tried to capture with complicated HMMs as intense bands of power at
frequencies below 2 bpm (beats per minute).
\nomenclature[rbpm]{bpm}{Beats per minute.}  In addition to those low
frequency oscillations, he noticed bands of power between 10 and 20
bpm in the spectrograms (See Fig.~\ref{fig:sgram}).  That higher
frequency power is evidence of respiration.  Using both of those
features, he classified the test data by hand.  First he classified
each entire record for \emph{event 1}.  Since almost none of the
minutes in the normal records are apnea, he classified each minute in
those records as normal.  His third attempt at \emph{event 2} was the
best entry at the close of the contest.

\begin{figure}
  \centering{\resizebox{\textwidth}{!}{\includegraphics{sgram.jpg}}
  }
  \caption[Information about respiration in high
  frequency phase variations]
  {Information about respiration in high frequency bands of power
    spectra.  This is derived from the $a11$ record between 20 minutes
    and 2 hours and 30 minutes after the start.  The upper plot is
    heart rate (bandpass filtered 0.09-3.66 bpm), the middle plot is a
    spectrogram of the heart rate, and the lower plot is the expert
    classification.  A single band of spectral power between about 10
    and 20 bpm without much power below the band in the spectrogram
    indicates normal respiration.}
  \label{fig:sgram}
\end{figure}

The question that motivated the contest is ``Is the information in an
ECG alone sufficient to classify apnea?'' McNames work answered the
question affirmatively.  For attempts to do the classification
automatically, his work suggests the following points:
\begin{enumerate}
\item Heart rate oscillations at about 1.3 bpm indicate apnea.
\item A clean band at about 14 bpm in the power spectrum indicates
  normal respiration.
\end{enumerate}

\afterpage{\clearpage}%% Print this right here please.

\section{Using HMMs to Address the Challenge}
\label{sec:apnea_hmms}

In the following sections we use HMMs to address the CINC2000
challenge.  We begin with an HMM scheme for estimating heart rate
because, like McNames, we find off-the-shelf QRS detection codes are
not adequate for the variety of the CINC2000 data.  From the estimated
heart rate signal for each record, we derive a sequence of two
dimensional observations.  The first component is the low pass
filtered heart rate, and the second is roughly the intensity of the
spectrogram in the range of respiration near 15 beats per minute.
Next we fit an HMM with vector autoregressive observation models as
described in Section~\ref{sec:ARVGaussian} to the two dimensional
observations derived from the training data records.  And finally we
apply that HMM to classify each minute of the test data.

\subsection{Estimating Heart Rate}
\label{sec:ecg_hmms}

\begin{figure}
  \centering{\resizebox{0.8\textwidth}{!}{\includegraphics{elgendi.pdf}}
  }
  \caption[Inadequate off-the-shelf ECG detectors]%
  {In this segment of the ECG for record a03, stars indicate estimates
    of the R wave locations by an off-the-shelf detector\cite{porr_2024_10573363},
    and $\times$s indicate estimates from the algorithm described in
    Section~\ref{sec:ecg_hmms}. }
  \label{fig:elgendi}
\end{figure}

\begin{figure}
  \centering{\resizebox{0.8\textwidth}{!}{\includegraphics{a03a10b03c02.pdf}}
  }
  \caption[Segments of ECGs from four records]%
  {Segments of ECGs from four records.  The ECGs for the different
    data records were so different from each other that off-the-shelf
    code was not adequate for estimating heart rate signals.}
  \label{fig:a03a10b03c02}
\end{figure}

\begin{figure}
  \centering{\resizebox{\textwidth}{!}{\includegraphics{constant_a03.pdf}}}
  \caption[Invariant PQRST shape]%
  {At different heart rates the shape and duration of the PQRST
    pattern doesn’t change. Only the delay between the sequences
    changes.  Notice that the lengths of the time intervals in the
    upper and lower plots are identical.}
  \label{fig:constant_a03}
\end{figure}

At first we tried using off-the-shelf code to extract heart rates from
the PhysioNet ECG data.  Figure~\ref{fig:elgendi} illustrates results
of using one of the detectors\cite{Elgendi2010} implemented by Porr et
al.\cite{py-ecg-detectors}, and Figure~\ref{fig:a03a10b03c02}
illustrates the challenging diversity of waveforms in the PhysioNet
data.  To address the diversity of waveforms, we developed an approach
to estimating heart rates from ECGs based on HMMs that relies on the
observation that for each record the shape and duration of the $PQRST$
pattern doesn't vary much.  For different heart rates the time between
the $T$ wave and the next $P$ wave does change, but the rest of the
pattern varies very little.  Figure~\ref{fig:constant_a03} illustrates
the invariance.

\subsubsection{State topology}
\label{sec:state_topology}

\begin{figure}
  \centering{\resizebox{0.5\textwidth}{!}{\input{ecg_hmm.pdf_t}}
  }
  \caption[Topology of HMMs for exploiting invariance of PQRST
  shapes]{Illustration of the topology of HMMs for exploiting
    invariance of PQRST shapes. The chain of 49 fast states models the
    invariant PQRST shape.  The variable duration paths through the
    slow states, $S_0$, $S_1$, and $S_2$, model the flexible time
    between $T$ and $P$ waves.}
  \label{fig:ecg_hmm}
\end{figure}

Figure~\ref{fig:ecg_hmm} illustrates the topology of the HMMs we built
to capture the invariant shapes of the PQRST sequences for each
individual record.  In addition to the states drawn, there is a
special \emph{outlier} state accommodates ECG-lead noise.  Here are
the essential characteristics of the topology:
\begin{itemize}
\item A loop of 52 discrete states.
\item A sequence of 49 \emph{fast} states that don't branch.  Each
  state $n$ in that sequence transitions exclusively to its successor
  $n+1$ at each time step.  These fast states model the unvarying
  PQRST shape.
\item Three \emph{slow} states that model heart rate variations.
  Each of these three states transitions to one of the following:
  \begin{itemize}
  \item Itself
  \item Its successor in the loop
  \item The first fast state
  \end{itemize}
\end{itemize}
The minimum number of states visited in a loop is 50, or 500~ms since
the ECG data was sampled at 100~Hz.  Consequently the model is not
appropriate for heart rates above 120~bpm.

\subsubsection{Observation models and training}
\label{sec:ecg_training}

We used a Gaussian scalar autoregressive observation model with means
for each state being a linear function of the previous three
observations with a fixed offset and variances fit to each state
separately.  The models are one dimensional versions of those
described in Section~\ref{sec:ARVGaussian}.

We used \emph{scipy.signal.find\_peaks} from \emph{SciPy} to supervise
training of an initial model for one of the records from CINC 2000.
We derived models for most\footnote{For a few of the records we used
  other techniques to obtain initial models.  However models for all
  records were trained similarly.} of the other records from that
initial model via unsupervised training.

\subsubsection{Applying trained ECG models}
\label{sec:applying_ECG_models}

As intended, the trained HMMs track PQRST shapes in the ECG data well.
Figure~\ref{fig:a01c02_states} illustrates the performance on two
records, \emph{a01} and \emph{c02}, in which the ECG shapes are quite
different.  To be clear, we repeat that we trained a separate HMM on
each record.
\begin{figure}
  \centering{\resizebox{0.8\textwidth}{!}{\includegraphics{a01c02_states.pdf}}
  }
  \caption[Decoded state sequences]{State sequences from Viterbi
    decoding of ECG signals for two records appear.  The invariant
    $PQRST$ pattern maps to the lines of constant slope.  The varying
    lengths of the horizontal segments accounts for variable time
    between heart beats.}
  \label{fig:a01c02_states}
\end{figure}

In addition to analyzing measured ECGs, the models can generate
simulated ECGs, eg, Figure~\ref{fig:simulated}.
\begin{figure}
  \centering{\resizebox{0.8\textwidth}{!}{\includegraphics{simulated.pdf}}
  }
  \caption[Simulated ECG]{A plausible ECG appears in the upper plot,
    and the corresponding state sequence appears in the lower plot.
    We created both plots by fitting a model to the \emph{a01} ECG
    data and then driving that model with a random number generator.}
  \label{fig:simulated}
\end{figure}

\subsubsection{Mapping state sequences to heart rate}
\label{sec:states2hr}

\begin{figure}
  \centering{\resizebox{0.8\textwidth}{!}{\includegraphics{ecg2hr.pdf}}
  }
  \caption[Heart rate derived from ECG.]{An illustration of using
    Viterbi decoding to derive heart rate from an ECG.  An ECG segment
    appears in the upper plot, and the corresponding segment of the
    decoded state sequence appears in the middle plot.  The $\times$
    marks in the middle plot indicate the times when the state is 31.
    The same times are also indicated in the upper plot.  The inverse
    of the difference in time between adjacent $\times$s provides the
    estimated heart rate.  A smoothed version of those estimates
    sampled periodically at 2 Hz appears in the lower plot. }
  \label{fig:ecg2hr}
\end{figure}

We built HMMs of ECG signals to estimate heart rate signals because we
believe that the timing of heart beats has the information that is
relevant for detecting apnea.  Decoded state sequences like those that
appear in Figure~\ref{fig:a01c02_states} are sufficient for estimating
heart rate.  Considering the circular character of the topology
depicted in Figure~\ref{fig:ecg_hmm}, the estimated heart rate is
simply the rate at which decoded sequences go around the circle, and
the phase of state sequence is not important for the estimation.  It
is nice however that one state in the chain of fast states corresponds
closely to the peak of the R wave in the ECGs as appears in
Figure~\ref{fig:ecg2hr}.

\afterpage{\clearpage}%% Print this right here please.

\subsection{An Observation Model and HMMs of Heart Rate}
\label{sec:apnea_observation_model}

While the previous section was about designing HMMs of ECG signals in
order to estimate heart rate signals, here we will address designing
HMMs of heart rate signals in order to detect apnea.  In
Section~\ref{sec:mcnames} we described two characteristics that
McNames used for classifying each minute, namely:
\begin{itemize}
\item Oscillations of the heart rate with periods of about 45 seconds.
  Such oscillations appear in Fig.~\ref{fig:a03erA}.
\item Modulation of the heart rate by respiration at around 15 beats
  per minute. Such modulation is captured by the spectrogram in
  Fig.~\ref{fig:sgram}.
\end{itemize}
We derive these two characteristics from heart rate signals (as appears
in Fig.~\ref{fig:ecg2hr}) sampled at 2 Hz using spectral techniques
that begin with a fast Fourier transform (FFT) of each entire record
of about 8 hours.  Figure~\ref{fig:explore} illustrates a transition into apnea
in which both characteristics indicate the transition.  The HMM we
built for detecting apnea has eleven states and uses fifth
order vector autoregressive observation models of two dimensional
observations, with components called \textbf{low pass heart rate} and
\textbf{respiration}.

\begin{figure}
  \centering{\resizebox{1.0\textwidth}{!}{\includegraphics{explore.pdf}}
  }
  \caption[Apnea characteristics derived from estimated heart rate
  signal.]{An illustration of apnea characteristic data derived from
    heart rate at a transition into apnea.  The expert marked a
    transition from normal respiration to apnea in record \emph{a03}
    at minute 427=7:07.  A low pass Gaussian filter applied to the raw
    heart rate signal produces the trace labeled \emph{Low Pass}, and
    the dots on that trace indicate the data passed as one component
    of the observation data to the HMM.  A Gaussian filter that passes
    frequencies in a band typical for respiration yields the
    \emph{Band Pass} trace.  The \emph{Respiration} trace is a low
    pass filtered version of the envelope of the \emph{Band Pass}
    trace.  Again, the dots indicate data passed to the HMM.  The
    characteristics of apnea are the large slow oscillations in the
    \emph{Low Pass} signal and the drops of the \emph{Respiration}
    signal to low levels.}
  \label{fig:explore}
\end{figure}

\begin{figure}
  \centering{\resizebox{1.0\textwidth}{!}{\includegraphics{errors_vs_fs.pdf}} }
  \caption[Error rate as a function of sample frequency.]{Performance
    on the training records (\emph{a b} and \emph{c} records excluding
    defective records) vs observations per minute. The models have
    eleven states and the observation models are vector autoregressive
    models for heart rate and respiration.}
  \label{fig:errors_vs_fs}
\end{figure}

Of the host of parameters, we chose the following for the observation
model by hand using one dimensional studies on the training data like
Figure~\ref{fig:errors_vs_fs}:
% \ArOrder et al come from apnea_values.tex
\begin{description}
\item[Autoregressive Order] \ArOrder
\item[Model Sample Frequency] $\ModelSampleFrequency$ samples per minute
\item[Low Pass Period] $\LowPassPeriod$ seconds
\item[Respiration Center Frequency] $\RespirationCenterFrequency$ bpm
\item[Respiration Filter Width] $\RespirationFilterWidth$ bpm
\item[Respiration Smoothing] $\RespirationSmoothing$ bpm
\end{description}
Figure~\ref{fig:explore} illustrates the derivation of HMM observation
data from the estimated raw heart rate signal using these parameters.

\subsection{Using a Sequence of Class Probabilities}
\label{sec:prettygood}

Equation \eqref{eq:wit}, ie,
\begin{equation*}
  w(t,s) \equiv P_{\ti{S}{t}\given \ts{Y}{0}{T}} \left(s\given \ts{y}{0}{T}
  \right),
\end{equation*}
provides the probability of each state at each time given the model and
all of the observations.  From such state weights, we construct a
sequence of ratios with
\begin{equation*}
  \ti{r}{t} \equiv \frac{\sum_{s\in A} w(t,s)}{\sum_{s\in N} w(t,s)}
\end{equation*}
where $A$ is the set of apnea states and $N$ is the set of normal
states.  Finally we derive a sequence of classifications,
$\ts{c}{0}{T}$, from the sequence of ratios, $\ts{r}{0}{T}$, by
comparing to a threshold, $R$,
\begin{equation}
  \label{eq:class_sequence}
  \ti{c}{t} =
  \begin{cases}
    N & \ti{r}{t} < R \\
    A & \ti{r}{t} >= R
  \end{cases}.
\end{equation}
A search over parameter values like that illustrated in
Figure~\ref{fig:errors_vs_fs} finds $R=\Threshold$ minimizes the number of
classification errors on the training data.

\section{Results}
\label{sec:results}

We trained an HMM with the structure and parameters described in
Section~\ref{sec:apnea_observation_model} on the training data (the
usable \emph{a}, \emph{b} and \emph{c} records) and then used it to
classify each minute of the training data as either normal, $N$, or
apnea, $A$.  The results appear in Table~\ref{tab:score}.  Results of
applying the HMM to the test data (the \emph{x} records appear in
Table~\ref{tab:test_score}.  We are disappointed that the score on the
test data does not place among the official entries in the \emph{CinC
  Challenge 2000 Top Scores} listed at
https://archive.physionet.org/challenge/2000/top-scores.shtml which
have error rates from 0.0738 to 0.1446.  While we were able to get
slightly better performance with more complex ideas, the small
improvements did not seem to justify the complexity.

\begin{table*}
  \centering
  \input{score.tex}
  \caption[Performance]{Performance of the HMM described in
    \ref{sec:apnea_observation_model} on the training data.}
  \label{tab:score}
\end{table*}

\begin{table*}
  \centering
  \input{test_score.tex}
  \caption[Performance]{Performance of the HMM described in
    \ref{sec:apnea_observation_model} on the test data.}
  \label{tab:test_score}
\end{table*}

\section{Classification Versus Estimation}
\label{sec:ClassVsEst}

In the early chapters of this book, we have emphasized choosing model
parameters to maximize the likelihood of the data, the tweaks and
fudges we've used in this chapter are concerned with improving
classification performance rather than improving likelihood.  While it
is true that if one had access to accurate probabilistic
characterizations of observations conditional on class membership the
best classifier would be a likelihood ratio classifier, it is not true
that without such characterizations the best approach to
classification is to estimate them.  This point was made forcefully by
Vapnik\cite{Vapnik98} who said, ``one should solve the
[classification] problem directly and never solve a more general
problem as an intermediate step.''

%%%
%%% Local Variables:
%%% TeX-master: "main"
%%% eval: (load-file "hmmkeys.el")
%%% mode: LaTeX
%%% End:
