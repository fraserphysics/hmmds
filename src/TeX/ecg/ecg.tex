% Notes on modeling ECGs with HMMs directly

\documentclass[12pt]{article}
\usepackage{graphicx,color}
\usepackage{amsmath, amsfonts}
\usepackage{placeins}

\newcommand{\field}[1]{\mathbb{#1}}
\newcommand{\INTEGER}{\field{Z}}
\newcommand{\REAL}{\field{R}}
\newcommand{\COMPLEX}{\field{C}}
\newcommand{\id}{\mathbb{I}}
\newcommand{\variance}[1]{\field{V}\left[ #1 \right]}
\newcommand{\normal}[2]{{\cal N}\left(#1,#2 \right)}
\newcommand{\argmax}{\operatorname*{argmax}}

\title{HMMs for ECGs}
\author{Andrew M.\ Fraser}

\begin{document}
\maketitle

\section{Introduction}
\label{sec:introduction}

I wrote explore.py to see how processing ECGs for apnea detection was
working.  Because I was disappointed by the performance of standard qrs
detectors that I got from
https://github.com/berndporr/py-ecg-detectors\footnote{After trying
  each of the detectors in that package I chose the one called
  \emph{Elgendi} }, I wrote my own by
building HMMs for ECG time series.

Recipes for making the models and figures described here appear in
Makefile and Rules.mk.  On 2023-04-12 building this document from
scratch took real 21m46.683s user 264m52.028s sys 5m38.888s.  On
2023-04-17 it took more than 4 hours because then I trained a
different HMM on each record.

\section{Outliers}
\label{sec:outliers}

While most of the ECG data consists of oscillations with amplitudes
less than $\pm$ 2mV, some intervals have spurious signals with $\pm$
10mV.  I call such signals \emph{lead noise}\footnote{The data in
  record \emph{C01} is less than 0.1mV till minute 5.1 when it goes to
  10 mV.  The usable data only starts at minute 10.24.}.  A model,
$\theta$, fit to the typical data can assign $P(y|\theta) = 0$ to lead
noise.  The observation models use inverse gamma priors for the
variance, and I use a single state, which I call the \emph{outlier}
state, with a prior that enforces a large variance to accommodate lead
noise.  In particular the parameters are $\alpha=10^{6}$ and
$\beta=10^{8}$ which in the reestimation formula
\begin{equation*}
  \sigma^2 = \frac{2 \beta + \text{data}}{2 \alpha +2 + \text{weight}}
\end{equation*}
gives $\sigma^2 \approx 100$.  It would take more than a million data
points to override that.

Since lead noise can start abruptly at any time, I allow transitions
to the outlier state from every other state.  It's important to ensure
that those transitions are infrequent.  I set the probability of
transitions\footnote{I found that values smaller than $10^{-165}$
  failed.} with the argument \emph{--noise\_parameters} of
model\_init.py to $10^{-50}$.

\section{A Model with a 49 State Chain}
\label{sec:mono20}

After trying many state topologies, I found that an HMM with a chain
of states that matches the ECG sequence PQRST with a fixed
duration\footnote{Each state in the chain has a single successor
  except the low probability transition to the outlier state.}
works for record a01 if it is trained on that record.  Viterbi
decoding with the model after training for 20 iterations on record
\emph{a01} yields Figures~\ref{fig:dict_states_70}
and~\ref{fig:dict_states_71}.  Note that the segments in the two
figures have the same duration, 0.04 minutes or 2.4 seconds, and that
they are separated by 10.5 seconds.  The doubling of the patient's
heart rate reflects an apnea cycle.  The HMM tracks the changing heart
rate without missing or inserting beats.

\begin{figure}
  \centering
    \resizebox{0.7\textwidth}{!}{\includegraphics{a01_trained_AR3_states_70.pdf}}
    \caption{Performance of the HMM with a single chain of 49 states
      trained to model PQRST sequences.  An short segment of the ECG
      signal appears in the upper plot, while the corresponding
      decoded state sequence appears in the lower plot.  The model was
      fit using 50 iterations of the Baum Welch algorithm.}
  \label{fig:dict_states_70}
\end{figure}

\begin{figure}
  \centering
  \resizebox{0.7\textwidth}{!}{\includegraphics{a01_trained_AR3_states_71.pdf}}
  \caption{Performance of the same model used for
    Fig.~\ref{fig:dict_states_70}, but looking at a different
    segment of the data.}
  \label{fig:dict_states_71}
\end{figure}

\section{Successful Models}
\label{sec:successful_models}

On 2023-03-31 I created a model that I trained on only the a01 record.
It worked surprisingly well on all of the other CINC-2000 records
except a19.  Here is a list of features of the code and model that I
think are important:
\begin{description}
\item[Joint Observation Model] My old code for training created
  special classes that I called \emph{bundles}.  I eliminated bundles
  and wrote a more general class called \emph{JointObservation} that
  can combine any number of component observation models of any type.
  I also wrote an observation class called \emph{ClassObservation}
  designed to be used as a component of a JointObservation.  A
  ClassObservation instance deterministically produces an observation
  that indicates the class of which the state is a member.  The
  reestimate method of ClassObservation does nothing.  Its parameters
  never change.
\item[Modeling Outliers] In a ClassObservation instance the sets of
  states defined by the classes constitute a disjoint and exhaustive
  partitioning.  To accommodate outliers in ECG data, I wrote a
  subclass of ClassObservation called \emph{BadObservation} that
  supports states that have positive likelihood for all classes.  I
  also wrote a subclass of HMM for which training does not modify the
  state transition probabilities.  Using that subclass, I set the
  probability of each other state transitioning to the single state
  that models outliers to be extremely small.
\item[Plausible Priors] Before I wrote the classes to model outliers
  specifically, I used priors that forced large variances for states
  that modeled outliers.  With the new classes, the priors give a
  variance of 100 for the outliers which is appropriate because the
  signal is clamped at $\pm 10$ mV.
\end{description}

\begin{figure}
  \centering
  \resizebox{1.2\textwidth}{!}{\includegraphics{a01a19c02.pdf}}
  \caption{Subsets of data from three CINC-2000 records.  The trace
    labeled \emph{c02 states} illustrates the performance of the model
    trained on a01 alone.  I am surprised that the same model fails on
    the ECG from a19 because that ECG seems more similar to a01 than
    c02.}
  \label{fig:a01a19c02}
\end{figure}

\begin{figure}
  \centering
  \resizebox{1.0\textwidth}{!}{\includegraphics{train_log.pdf}}
  \caption{Iterative training progress.  Each trace is the log of a
    quantity divided by the number of training samples.}
  \label{fig:train_log}
\end{figure}

\begin{figure}
  \centering
  \resizebox{1.0\textwidth}{!}{\includegraphics{like_a14_x07.pdf}}
  \caption{Plots of $\log\left(p(y[t]|y[:t]) \right)$ for records a14
    and x07.  Those records have the high and low likelihoods in
    Table~\ref{tab:cross_entropy}.}
  \label{fig:likelihood}
\end{figure}

\begin{table}
  \centering
    \input{table.tex}
  \caption[Cross Entropy]{Cross entropy and fraction of each record
    that is plausible for a model trained on record a01.}
  \label{tab:cross_entropy}
\end{table}

\begin{figure}
  \centering
  \resizebox{1.0\textwidth}{!}{\includegraphics{simulated.pdf}}
  \caption{A simulated ECG generated by a the model trained on record a01.}
  \label{fig:simulated}
\end{figure}

\subsection{Models Fit to Only One Record}
\label{sec:selves}

The statistics in Table~\ref{tab:self_cross_entropy} are drawn from
models that were each trained on one record and then evaluated on that
same record.  I call those models self-trained.  A model fit to record
a01 was the seed for each of the self-trained models\footnote{I found
  that records a12 and c07 required custom initialization.  See
  details in Rules.mk}.  I used results from scipy.signal.find\_peaks to
initially supervise the training of the seed model.

\begin{table}
  \centering
    \input{self_table.tex}
  \caption[Cross Entropy]{Cross entropy and fraction of each record
    that is plausible for models trained the record itself.}
  \label{tab:self_cross_entropy}
\end{table}

Here are some comments on the self-trained model including comparison
to heart rate calculated using the Elgendi detector in the
py-ecg-detector package:
\begin{description}
\item[Good Overall] For record a20 is which is at the bottom of
  Table~\ref{tab:self_cross_entropy} the statistics improve with
  $-h(X|\theta): -1.3079 \rightarrow 1.4106$ and 92.967\% $\rightarrow$
  99.887\% plausible.  The self-trained a20 model is better than Elgendi
  most of the time, but in the noisy patches the slow states yield
  unreasonably low heart rates.
\item[x20] The x20 model finds 98.238\% of the record plausible.  It
  is one of only two model-record combination in which that score is
  below 99\%.  Leads disconnected for about 15 minutes starting at
  about 173.  Starting at 387 -- 410 noisy spell that model handles
  better than Elgendi
\item[x29] At 98.625\%, the x29 model also scores badly for
  plausibility.  The leads are not connected for the first 18 minutes.
  The model preforms adequately in a difficult interval from 117 --
  123 and again around 295.  For long intervals in the record there
  are pairs of beats (with three to five normal beats in between) that
  are separated by only 0.6 seconds.  So the estimated heart rate
  jumps from 100 to 40 between samples.  Both Elgendi and the
  self-trained model track those changes.
\item[x27] Has high cross entropy and high plausibility.  Looking at
  plots, I think it performs well.  Perhaps the high cross entropy is
  because the signal amplitude is large (normal peaks above 4mV).
\item[a01-a10] At 3:00 a06 inserts extra beats
\item[a11-a20] I must fix a12.  It inserts many extra beats.  a19
  drops beats at 32 places starting at 163.
\item[b01-b04] b01 lots of extra beats at 9.  At 383.16 there's a beat
  that's too short for the model (interesting but not a problem).  b03
  misses a beat at 23.3 and inserts several between 23.6 and 24.0.
  b03 has several errors that I don't understand.  b04 needs work
  because it inserts beats in many places.  b05 does not exist.
\item[c01-c10] Extra beats in c01 between 10 and 26, but otherwise it
  looks good.  c02 looks good (inverted R signal).  c03: extra beats
  at 0.3, 2, 6, 36.  Perhaps it would be better if it was easier to go
  to the outlier state.  c04: extra beats at 21.87, 30 - 30.5.  c06 2
  extra beats at 91.9, 111.8, 144.  c07 misses every other beat.  It
  is unusable.
\item[x01-x10] x04: extra beats at 44.5
\item[x11-x20] x13: Model says ECG negative excursions are beats;
  likelihood drops.  x17: Elgendi double marks beats.  HMM seems OK.  x18
  good data starts at 14.85.  x19 HMM and Elgendi disagree and HMM is
  right.
\item[x21-x30] x26 has pathological negative peaks.
\item[x31-x35] All are OK
\item[Fixed] a12 and c07.  The R peaks go negative for a12 from -.2mV
  to -.9mV.  The peaks of c07 range from .3 mV to .7mV.  Using a scale
  factor for the peak finding code in utilities.read\_ecgs, I trained
  custom initial models for both.  Those custom models matched most of
  the PQRST sequences well.  However, a12 is noisy and that affects
  the phase of many of the estimated peaks.  I addressed that noise
  induced phase shift in a12 by reducing the number of slow states
  from 10 to 2.  That forces the two slow states to have higher
  variance which in turn makes decoding more likely to attribute noisy
  patches to the slow states.
\item[Could Improve] The ``problem'' with x26 is its treatment of the
  frequent pathological negative peak.  I think I could/should deal
  with them at a higher level.  The ECG signals for a12 and b04 are
  noisy.  Look at b04 times 27.4 to 29.0.
\item[Ideas] Discard low likelihood intervals.  Bad regions seem to have low
integrated log likelihood.  Low likelihood in slow states indicates
noise.  Feed smoothed likelihood to apnea detector as proxy for noise
level and low confidence in heart rate.
\end{description}

The x12 ECG is noisy.  The most annoying model pathology is that the
fast states are decoded in the inter-beat interval and the R peak is
put in the next inter-beat interval.  See the peak at 201.825.
Varying the AR order didn't help.  In fact AR1 misses every other
beat.  I found that reducing the number of slow states helped.  My
guess is that with fewer slow states, the output models for those
states have larger variance and are thus happy to absorb the high
noise periods.

\section{Quadratic Fit to Peak}
\label{sec:quadratic}

Here I find an estimate of the location of a peak based data sampled
periodically.  I fit a quadratic function to a local maximum and its
two neighbors.  WLOG the (x,y) sample pairs are $[(-d,a), (0,b),
(d,c)]$, and the quadratic function is
\begin{equation*}
  y(x) = \alpha x^2 + \beta x + \gamma.
\end{equation*}
Some manipulation yields:
\begin{align*}
  \alpha &= \frac{a+c-2b}{2d^2} \\
  \beta &= \frac{c-a}{2d} \\
  \gamma &= b \\
  \frac{d}{dx} y(x) &= 2x\alpha + \beta
\end{align*}
So the quadratic estimate of the peak location is
\begin{equation*}
  \argmax_x y(x) = \frac{-\beta}{2\alpha} = \frac{d(a-c)}{2(a+c-2b)}.
\end{equation*}

\section{To Do}
\label{sec:todo}

\begin{itemize}
\item Clean up the code.  Change name \emph{bad} to \emph{outlier}
\item Eliminate prior for outlier state.  Simply hold it constant in
  reestimation.
\item Recover old explore.py and return to work on apnea detection.
\item model\_init.py
\item Write scoring code.  Find A files that don't work with first
  model and develop model for them.
\item States with tied outputs.
\end{itemize}

\begin{itemize}
\item Plot training characteristics
\end{itemize}

Remove build directory and run, \emph{time make -j 10} to build this
document:\\
%
real	204m10.921s\\
%
user	3173m12.903s\\
%
sys	45m56.637s

\end{document}
