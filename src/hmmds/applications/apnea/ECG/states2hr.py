"""states2hr.py: Map a decoded state sequence to a heart rate time
series

python states2hr.py states_file heart_rate_file

"""

import sys
import os.path
import argparse
import pickle

import pint
import numpy
import numpy.fft

import hmmds.applications.apnea.ECG.utilities

PINT = pint.UnitRegistry()


def parse_args(argv):
    """ Convert command line arguments into a namespace
    """

    parser = argparse.ArgumentParser(
        description='Calculate heart rate from decoded states')
    hmmds.applications.apnea.ECG.utilities.common_arguments(parser)
    parser.add_argument(
        '--likelihood',
        type=str,
        nargs=2,
        help='[path to p(y[t]|y[:t]),  fraction of data to censor]')
    parser.add_argument('--ecg_peaks',
                        type=str,
                        nargs=2,
                        help='[path to ecg,  fraction of data to censor]')
    parser.add_argument('--hr_dips',
                        type=float,
                        help='Drop long interbeat intervals')
    parser.add_argument('--samples_per_minute', type=int, default=10)
    parser.add_argument('--r_state',
                        type=int,
                        default=20,
                        help='state index for R peak')
    parser.add_argument('--outlier_state',
                        type=int,
                        default=0,
                        help='state index for outliers')
    parser.add_argument('states_file', type=str, help='Path to decoded states')
    parser.add_argument('heart_rate_file',
                        type=str,
                        help='Path to heart rate data')
    args = parser.parse_args(argv)
    hmmds.applications.apnea.ECG.utilities.join_common(args)
    return args


def find_beats_and_periods(states: numpy.ndarray, r_state=35, outlier_state=0):
    """Args:
        states: State sequence from Viterbi decoding
        r_state: Defines beat time
        outlier_state: The state for ECG samples that are implausible.

    Return: The pair (array of integer times and periods, set of bad
    intervals)

    bad_intervals indicate atypical or implausible signal segments.
    Elements of bad_intervals are integer beat times at the start such
    segments.

    """
    bad_intervals = set()

    r_indices = numpy.nonzero(states == r_state)[0]
    time_period = numpy.empty((len(r_indices) - 1, 2), dtype=int)
    time_period[:, 0] = r_indices[:-1]  # The times
    time_period[:, 1] = r_indices[1:] - r_indices[:-1]  # The periods

    # If outlier_state occurs in an interval, it is bad
    for (time, period) in time_period:
        if numpy.count_nonzero(states[time:time + period] == outlier_state) > 0:
            bad_intervals.add(time)
    return time_period, bad_intervals


def censor_likelihood(time_period, bad_intervals, likelihood, censor_fraction):
    """Censor times with low likelihood

    Args:
        time_period: time_period[i,0] = integer time of beat, time_period[i,1] is period
        bad_itervals: Set of integer times to censor
        likelihood: p(y[t]|y[:t])
        censor_fraction: Fraction of intervals to reject based on likelihood
    """
    period_likelihood = numpy.empty(len(time_period))
    for i, (time, period) in enumerate(time_period):
        period_likelihood[i] = likelihood[time:time + period].min()
    sorted_ = period_likelihood.copy()
    sorted_.sort()
    threshold = sorted_[int(censor_fraction * len(sorted_))]
    bad_intervals.update(
        time_period[numpy.nonzero(period_likelihood < threshold)[0], 0])
    return time_period, bad_intervals


def censor_peak(time_period, bad_intervals, ecg, peak_fraction):
    """Censor times with bad prominence

    Args:
        time_period: time_period[i,0] = integer time of beat, time_period[i,1] = period
        bad_itervals: Set of integer times to censor
        ecg: Time series
        peak_fraction: Fraction of intervals to reject based on peak
    """
    if peak_fraction < 0:
        ecg = ecg.copy() * -1
        peak_fraction *= -1

    peak = numpy.array([ecg[t:t + d].max() for t, d in time_period])
    pointers = numpy.argsort(peak)
    threshold = peak[pointers[int((1 - peak_fraction) * len(pointers))]]

    indices = numpy.nonzero(peak > threshold)[0]
    bad_intervals.update(time_period[indices, 0])
    return time_period, bad_intervals


def censor_dips(time_period, bad_intervals, min_hr):
    """Censor times with long periods

    Args:
        time_period: time_period[i,0] = integer time of beat, time_period[i,1] is period
        bad_itervals: Set of integer times to censor
        min_hr: Minimum heart rate in beats per minute
    """
    max_period = 60 * 100 / min_hr
    indices = numpy.nonzero(time_period[:, 1] > max_period)[0]
    bad_intervals.update(time_period[indices, 0])
    return time_period, bad_intervals


def calculate(
    time_period: numpy.ndarray,
    bad_intervals: set,
    time_out,
    in_frequency=100 * PINT('Hz'),
    out_frequency=2 * PINT('Hz')) -> dict:
    """Calculate heart rate and low pass heart rate

    Args:
        time_period: Integer beat times and integer periods
        bad_intervals: Subset of beat times with untrusted subsequent intervals
        time_out: Pint time for length of result
        in_frequency:
        out_frequency:

    Return: Dict of heart rate time series and its sample frequency
    with keys 'sample_frequency' and 'hr'.

    """

    # Create periods, an array of periods that is uniformly sampled at
    # out_frequency
    n_out = int((out_frequency * time_out).to('').magnitude)
    n_in = len(time_period)
    # Number of samples of inverse heart rate
    out_periods = numpy.empty(n_out)

    index_in = 0
    next_time = 0
    next_period = 0
    last_time = 0
    last_period = 0
    dp_dt = 0

    def next_good_period():
        """Find the next good interval

        Update values of index_in, last_time, next_time, last_period,
        next_period

        """
        nonlocal last_time, next_time, last_period, next_period, index_in, dp_dt
        for try_in in range(index_in, n_in):
            time, period = time_period[try_in]
            if time not in bad_intervals:
                last_time = next_time
                last_period = next_period
                next_time = time
                next_period = period
                index_in = try_in + 1
                dp_dt = (next_period - last_period) / (next_time - last_time)
                return
        index_in = n_in
        last_time = next_time
        last_period = next_period
        dp_dt = 0

    next_good_period()
    assert index_in < n_in

    # Assign first out_periods
    for k_out in range(n_out):
        if k_out / out_frequency >= next_time / in_frequency:
            k_start = k_out
            break
        out_periods[k_out] = next_period

    # Make values of last_* and next_* valid
    next_good_period()
    assert index_in < n_in

    # Assign a value to periods[k_out] for every k_out
    for k_out in range(k_start, n_out):
        if k_out / out_frequency >= next_time / in_frequency:
            next_good_period()
            if index_in >= n_in:
                for k in range(k_out, n_out):
                    out_periods[k] = last_period
                break
        t_k = k_out * in_frequency / out_frequency
        out_periods[k_out] = last_period + (t_k - last_time) * dp_dt

    # Return results in beats per minute
    out_periods /= in_frequency
    return {
        'sample_frequency': out_frequency,
        'hr': (1.0 / out_periods).to('1/minute')
    }


def main(argv=None):
    """Derive periodic time series of heart rate estimates from state sequences
    """

    if argv is None:
        argv = sys.argv[1:]
    args = parse_args(argv)
    with open(args.states_file, 'rb') as _file:
        states = pickle.load(_file)

    input_sample_frequency = 100 * PINT('Hz')
    time_out = len(states) / input_sample_frequency

    time_period, bad_intervals = find_beats_and_periods(states, args.r_state)

    if args.likelihood:
        with open(args.likelihood[0], 'rb') as _file:
            likelihood = pickle.load(_file)
        fraction = float(args.likelihood[1])
        time_period, bad_intervals = censor_likelihood(time_period,
                                                       bad_intervals,
                                                       likelihood, fraction)

    if args.ecg_peaks:
        ecg_path = os.path.join(args.ecg_dir, args.ecg_peaks[0])
        with open(ecg_path, 'rb') as _file:
            _dict = pickle.load(_file)
        ecg = _dict["ecg"]
        fraction = float(args.ecg_peaks[1])
        time_period, bad_intervals = censor_peak(time_period, bad_intervals,
                                                 ecg, fraction)

    if args.hr_dips:
        time_period, bad_intervals = censor_dips(time_period, bad_intervals,
                                                 args.hr_dips)

    hr_dict = calculate(time_period, bad_intervals, time_out)
    with open(args.heart_rate_file, 'wb') as _file:
        pickle.dump(hr_dict, _file)
    return 0


if __name__ == "__main__":
    sys.exit(main())

#Local Variables:
#mode:python
#End:
