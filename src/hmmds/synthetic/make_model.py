""" MakeModel.py: Make HMM for figure on cover

 EG. python MakeModel.py 50 data_dir lorenz.4 m12s.4y
"""

import sys
import os.path
import pickle
import itertools
import argparse

import numpy
import numpy.random

import hmm.base
import hmm.simple


def parse_args(argv):
    """Parse the command line
    """
    parser = argparse.ArgumentParser(description="Make HMM for figure on cover")
    parser.add_argument('--random_seed', type=int, default=0)
    parser.add_argument('--nstates',
                        type=int,
                        default=12,
                        help="number of hidden states")
    parser.add_argument('n_iterations',
                        type=int,
                        help="number of training iterations")
    parser.add_argument('data_dir',
                        type=str,
                        help="Directory for data and model")
    parser.add_argument('data_file', type=str)
    parser.add_argument('model_file', type=str)
    return parser.parse_args(argv)


def skip_header(_file):
    """Skip lines that start with #.
    Args:
        _file: An open file object

    """
    return itertools.dropwhile(lambda line: line.startswith("#"), _file)


def read_data(data_dir, data_file):
    """Read quantized data and return as numpy array.  Shift values by -1
    so that minimum for plots can be 1 while still using [0,n) for hmm
    code.

    """
    with open(os.path.join(data_dir, data_file), encoding='utf-8',
              mode='r') as file_:
        y = numpy.array([int(line) for line in skip_header(file_)], numpy.int32)
    return y, y.max() + 1


def main(argv=None):
    """Call with arguments: n, data_dir, data_file, model_file

    n = number of iterations

    data_dir = directory for data and resulting model

    data_file = name of data file

    model_file = name of the file into which resulting model to be written
    """

    if argv is None:  # Usual case
        argv = sys.argv[1:]

    args = parse_args(argv)

    y_data, cardy = read_data(args.data_dir, args.data_file)

    # Set random values of initial model parameters
    rng = numpy.random.default_rng(args.random_seed)
    p_state_initial = hmm.simple.Prob(rng.random(
        (1, args.nstates))).normalize()[0]
    p_state_time_average = hmm.simple.Prob(rng.random(
        (1, args.nstates))).normalize()[0]
    p_state2state = hmm.simple.Prob(rng.random(
        (args.nstates, args.nstates))).normalize()
    p_state2y = hmm.simple.Prob(rng.random((args.nstates, cardy))).normalize()

    # Train the model
    y_mod = hmm.simple.Observation(p_state2y, rng)
    mod = hmm.base.HMM(p_state_initial, p_state_time_average, p_state2state,
                       y_mod, rng)
    mod.train(y_data, args.n_iterations)

    # Strip and then save model in <model_file>
    mod.strip()
    with open(os.path.join(args.data_dir, args.model_file), mode='wb') as _file:
        pickle.dump(mod, _file)
    return 0


if __name__ == "__main__":
    sys.exit(main())

#Local Variables:
#mode:python
#End:
