r"""log_likelihood.py

Check likelihood of data from the state space model defined in
linear_map_simulatinon.py

"""

import argparse
import sys
import pickle

import numpy
import numpy.random

from hmmds.synthetic.filter import linear_map_simulation


def parse_args(argv):
    """Parse the command line.
    """

    parser = argparse.ArgumentParser(description='Illustrate MLE.')
    linear_map_simulation.system_args(parser)

    parser.add_argument('--n_samples',
                        type=int,
                        default=100000,
                        help='Number of samples')
    parser.add_argument('--n_b',
                        type=int,
                        default=10,
                        help='Number of b values')
    parser.add_argument('--b_range',
                        type=float,
                        nargs=2,
                        default=[.75, 1.25],
                        help='Fractional range of b values')
    parser.add_argument('--random_seed', type=int, default=9)
    parser.add_argument('data', type=str)
    return parser.parse_args(argv)


def main(argv=None):
    """Make data for a plot of log likelihood vs b

    """

    if argv is None:
        argv = sys.argv[1:]
    args = parse_args(argv)

    rng = numpy.random.default_rng(args.random_seed)

    # Make the nominal model and simulate the data
    # dt is OK pylint: disable = invalid-name
    dt = 2 * numpy.pi / (args.omega * args.sample_rate)
    # Code in linear_map_simulation.py makes a system and uses a
    # formula to calculate the stationary distribution.
    system, initial_dist = linear_map_simulation.make_linear_stationary(
        args, dt, rng)
    x_data, y_data = system.simulate_n_steps(initial_dist, args.n_samples)

    # Calculate the log likelihood for models with a range of b
    # (system noise) values
    b_array = numpy.linspace(args.b_range[0], args.b_range[1],
                             args.n_b) * args.b
    log_like = numpy.empty(b_array.shape)
    for i, b_i in enumerate(b_array):
        args.b = b_i
        system_b, b_dist = linear_map_simulation.make_linear_stationary(
            args, dt, rng)
        log_like[i] = system_b.log_likelihood(b_dist, y_data)

    with open(args.data, 'wb') as _file:
        pickle.dump(
            {
                'b_array': b_array,
                'log_like': log_like,
                'x_data': x_data,
            }, _file)
    return 0


if __name__ == "__main__":
    sys.exit(main())

#---------------
# Local Variables:
# mode:python
# End:
