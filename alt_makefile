# alt_makefile Like Makefile but for other documents.  Avoid cluttering Makefile

# Look at: https://makefiletutorial.com/

ROOT:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
TEX = $(ROOT)/src/TeX
PLOTSCRIPTS = $(ROOT)/src/plotscripts
HMMDS = $(ROOT)/src/hmmds
BUILD = $(ROOT)/build
XFIGS = $(PLOTSCRIPTS)/xfigs
ApneaPlotScripts = $(PLOTSCRIPTS)/apnea

# Default target
$(BUILD)/TeX/filter/filter.pdf:

# Rules for making plots
include $(PLOTSCRIPTS)/filter/Rules.mk
include $(PLOTSCRIPTS)/laser/Rules.mk

# Rules for making data files
include $(HMMDS)/synthetic/filter/Rules.mk
include $(HMMDS)/applications/laser/Rules.mk

# Rules for making documents
include $(TEX)/filter/Rules.mk

######################Target Documents##########################################
## ds21.pdf                       : Slides for 2021 SIAM Dynamical Systems meeting
.PHONY : ds21.pdf
ds21.pdf : $(TEX)/ds21/slides.pdf

$(TEX)/ds21/slides.pdf:
	cd $(TEX)/ds21 && $(MAKE) slides.pdf

$(TEX)/bundles.pdf: $(TEX)/bundles.tex  $(INTRODUCTION_FIGS) $(BASIC_ALGORITHMS_FIGS) $(APNEA_FIGS)
	cd TeX && $(MAKE) bundles.pdf

## variables     : Print selected variables.
.PHONY : variables
variables:
	@echo INTRODUCTION_FIGS: $(INTRODUCTION_FIGS)
	@echo BASIC_ALGORITHMS_FIGS: $(BASIC_ALGORITHMS_FIGS)
	@echo APNEA_FIGS: $(APNEA_FIGS)
	@echo In root Makefile, ROOT: $(ROOT)

# Local Variables:
# mode: makefile
# End:
